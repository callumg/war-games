# War Games

War Games is a simple simulation games where two armies fight. The game has a simple UI and the ability to create your own armies or import ones that are already created. This game was created as the main project in the course IDATT2001 at NTNU. It is available as a full project or as a standalone JAR file.

# Installation Guide
To install the program you can either [download the excecutable fat-jar](https://drive.google.com/file/d/1RDOBr9hDr_C6YplayetKv-tVKDcpS8xa/view?usp=sharing) or by cloning the repository. Note that by running the jar, a database file will be created in the same file location.


# Clone and run the repository
You will need git and Maven to be able to clone and run the program on your own computer.

To clone the repository type:

`$ git clone https://gitlab.stud.idi.ntnu.no/callumg/war-games`

Once cloned, type:

`$ cd war-games && mvn javafx:run `

to run the program.
