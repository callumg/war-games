package edu.ntnu.idatt2001.callumg.client;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * View util is used to help with visual tasks
 * that are used among multiple classes in the client.
 * It is primarily used to create popups and create stages.
 *
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public class ViewUtil {

    private static Alert alert;
    private static String css = "styles.css";

    /**
     * Static method to create stages.
     * The method gets the FXML file to set, sets a css file
     * and sets the icon for the application.
     *
     * @param fileName the fxml file name
     * @param stage    the stage to be created
     */
    public static void createStage(String fileName, Stage stage) {
        FXMLLoader fxmlLoader = new FXMLLoader(ViewUtil.class.getClassLoader().getResource(fileName + ".fxml"));
        try {
            stage.setScene(new Scene(fxmlLoader.load()));
            Image icon = new Image("logo.PNG");
            stage.getIcons().add(icon);
            stage.getScene().getStylesheets().add(css);
        } catch (IOException | IllegalStateException e) {
            giveError("Something went wrong when opening the window");
        }
    }

    /**
     * Helper method for regex in text fields.
     * @param regexType the type of text allowed.
     * @param textField the text field to apply to.
     */
    public static void addRegexHandler(final TextField textField, final String regexType) {
        String[][] regexTypes = new String[][] { { "No special characters", "\\a-zA-Z0-9*", "[^a-zA-Z0-9]" }, { "Only numbers", "\\d*", "[^\\d]" } };
        textField
            .textProperty()
            .addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                        Arrays
                            .stream(regexTypes)
                            .forEach(list -> {
                                if (list[0].equals(regexType)) {
                                    if (!newValue.matches(list[1])) {
                                        textField.setText(newValue.replaceAll(list[2], ""));
                                    }
                                }
                            });
                    }
                }
            );
    }

    /**
     * Helper method for max-length in text fields.
     * @param maxLength the max length of text allowed.
     * @param textField the text field to apply to.
     */
    public static void addTextLimiter(final TextField textField, final int maxLength) {
        textField
            .textProperty()
            .addListener(
                new ChangeListener<String>() {
                    @Override
                    public void changed(final ObservableValue<? extends String> ov, final String oldValue, final String newValue) {
                        if (textField.getText().length() > maxLength) {
                            String s = textField.getText().substring(0, maxLength);
                            textField.setText(s);
                        }
                    }
                }
            );
    }

    /**
     * Give information.
     * Static method that can be called by the respective controllers.
     * Method copied from IDATT1002 project.
     *
     * @param message the information message
     */
    public static void giveInformation(String message) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.show();
    }

    /**
     * Give error.
     * Static method that can be called by the respective controllers.
     * Method copied from IDATT1002 project.
     *
     * @param message the error message
     */
    public static void giveError(String message) {
        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    /**
     * Gets confirmation.
     * Static method that can be called by the respective controllers.
     * Method copied from IDATT1002 project.
     *
     * @param message the confirmation message
     * @return Boolean the confirmation
     */
    public static boolean getConfirmation(String message) {
        alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText(null);
        alert.setContentText(message);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == ButtonType.OK;
    }

    /**
     * Gives log entry.
     * Static method that can be called by the controllers.
     *
     * @param message the information message
     */
    public static void giveLogEntry(String message) {
        Dialog dialog = new Dialog();
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);
        dialog.setTitle("Log Entry");
        dialog.setHeaderText("Battle Log");
        dialog.setContentText(message);
        dialog.showAndWait();
    }

    /**
     * Gives information on how to use WarGames.
     * Static method that can be called by the controllers.
     */
    public static void giveWarGamesInformation() {
        Dialog dialog = new Dialog();
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);
        dialog.setTitle("War Games Information");
        dialog.setHeaderText("War Games Information");
        dialog.setContentText(
            "To start a battle, first import armies from csv files or  with armies." +
            "\n\n" +
            "If you have no armies you can create a new one in the manage armies popup." +
            "\n\n" +
            "You can edit troops in an army by double clicking it in the army troops tab." +
            "\n\n" +
            "Right clicking a unit in this list, you will be able to delete it." +
            "\n\n" +
            "After a battle has been simulated you can double click log entries to enlarge them."
        );
        dialog.showAndWait();
    }
}
