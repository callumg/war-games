package edu.ntnu.idatt2001.callumg.client.controller;

import edu.ntnu.idatt2001.callumg.client.App;
import edu.ntnu.idatt2001.callumg.client.UnitSingleton;
import edu.ntnu.idatt2001.callumg.client.ViewUtil;
import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.Battle;
import edu.ntnu.idatt2001.callumg.model.database.ArmyDAO;
import edu.ntnu.idatt2001.callumg.model.database.UnitDAO;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;

/**
 *  * Controller class for the main stage.
 *  * This controller handles events and
 *  * information shown on the warGames fxml file.
 *  *
 *  * @author Callum Gran
 *  * @version 1.7 28.05.2022
 */
public class WargamesController implements Initializable {

    @FXML
    private ComboBox<TerrainType> chooseTerrainCombo;

    @FXML
    private ComboBox<Army> chooseArmyOneCombo, chooseArmyTwoCombo;

    @FXML
    private Button battleBtn, resetBtn, editArmyBtn;

    @FXML
    private Label armyOneNameLabel, armyTwoNameLabel;

    @FXML
    private ListView<Unit> armyOneUnitsListView, armyTwoUnitsListView;

    @FXML
    private ListView<String> battleLogListView, battleStatsListView, armyOneStatsListView, armyTwoStatsListView;

    private Army[] armies;
    private ListView[] armyStatListViews;
    private Label[] armyNameLabels;
    private ListView<Unit>[] armyUnitsListViews;
    private ComboBox<Army>[] chooseArmyComboBoxes;

    private Unit selectedUnit;
    private int selectedIndex;
    private ObservableList<Army> allArmies;
    private ArmyDAO armyDAO = new ArmyDAO();
    private UnitDAO unitDAO = new UnitDAO();

    /**
     * Method to show update and show information of a battle
     * between two armies.
     * Shows error if no army is selected or the army is null.
     */
    @FXML
    void battle() {
        try {
            Battle battle = new Battle(armies[0], armies[1], chooseTerrainCombo.getValue());
            clearLists();
            ViewUtil.giveInformation(battle.simulate().toString() + " won!");
            battleLogListView.setItems(battle.getBattleLog());
            battleLogListView.scrollTo(battleLogListView.getItems().size());
            battleStatsListView.setItems(battle.getBattleStats());
            armies[0].getArmyUnitInfo().forEach(string -> armyOneStatsListView.getItems().add(string));
            armies[1].getArmyUnitInfo().forEach(string -> armyTwoStatsListView.getItems().add(string));
            toggleButtons(true);
        } catch (IllegalArgumentException | NullPointerException e) {
            ViewUtil.giveError(e.getMessage());
        }
    }

    /**
     * Method that allows opens window with the ability to manage your armies.
     */
    @FXML
    void manageArmies() {
        App.editArmy();
        updateArmyDropSelector();
    }

    /**
     * Method to give popup with information.
     */
    @FXML
    void warGamesInformation() {
        ViewUtil.giveWarGamesInformation();
    }

    @FXML
    void selectArmyOne() {
        if (chooseArmyOneCombo.getValue() != null) {
            armies[0] = new Army(chooseArmyOneCombo.getValue().getId(), chooseArmyOneCombo.getValue().getName(), chooseArmyOneCombo.getValue().getAllUnits());
            armyNameLabels[0].setText(armies[0].getName());
            showArmy(0);
        }
    }

    @FXML
    void selectArmyTwo() {
        if (chooseArmyTwoCombo.getValue() != null) {
            armies[1] = new Army(chooseArmyTwoCombo.getValue().getId(), chooseArmyTwoCombo.getValue().getName(), chooseArmyTwoCombo.getValue().getAllUnits());
            armyNameLabels[1].setText(armies[1].getName());
            showArmy(1);
        }
    }

    /**
     * Method to reset armies to original state.
     */
    @FXML
    void reset() {
        clearLists();
        updateArmyDropSelector();
        showArmy(0);
        showArmy(1);
        toggleButtons(false);
    }

    /**
     * Helper method to make the dropdown have the correct values.
     */
    private void updateArmyDropSelector() {
        int selectedIndexOne = chooseArmyOneCombo.getSelectionModel().getSelectedIndex();
        int selectedIndexTwo = chooseArmyTwoCombo.getSelectionModel().getSelectedIndex();
        try {
            allArmies = FXCollections.observableList(armyDAO.getArmies());
        } catch (SQLException | IllegalArgumentException e) {
            ViewUtil.giveError(e.getMessage());
        }
        chooseArmyOneCombo.setItems(allArmies);
        chooseArmyTwoCombo.setItems(allArmies);
        if (selectedIndexOne != -1) {
            if (selectedIndexOne >= chooseArmyOneCombo.getItems().size()) {
                chooseArmyOneCombo.setValue(null);
                armies[0] = null;
                armyStatListViews[0].getItems().clear();
                armyUnitsListViews[0].getItems().clear();
            }
            else chooseArmyOneCombo.setValue(allArmies.get(selectedIndexOne));
        }
        if (selectedIndexTwo != -1 ) {
            if (selectedIndexTwo >= chooseArmyTwoCombo.getItems().size()) {
                chooseArmyTwoCombo.setValue(null);
                armyStatListViews[1].getItems().clear();
                armyUnitsListViews[1].getItems().clear();
            }
            else chooseArmyTwoCombo.setValue(allArmies.get(selectedIndexTwo));
        }
    }

    /**
     * Helper method to disable and enable buttons.
     * This method is used so the user cannot
     * cause unwanted errors or crash the program.
     * @param isEnabled boolean to show if the buttons are to be enabled.
     */
    private void toggleButtons(Boolean isEnabled) {
        chooseTerrainCombo.setDisable(isEnabled);
        battleBtn.setDisable(isEnabled);
        editArmyBtn.setDisable(isEnabled);
        chooseArmyOneCombo.setDisable(isEnabled);
        chooseArmyTwoCombo.setDisable(isEnabled);
        resetBtn.setDisable(!isEnabled);
    }

    /**
     * Helper method to show army information.
     * @param armyNumber the number of the army to show.
     */
    private void showArmy(int armyNumber) {
        armyStatListViews[armyNumber].getItems().clear();
        armyNameLabels[armyNumber].setText(armies[armyNumber].getName());
        armyUnitsListViews[armyNumber].setItems((ObservableList<Unit>) armies[armyNumber].getAllUnits());
        armies[armyNumber].getArmyUnitInfo().forEach(string -> armyStatListViews[armyNumber].getItems().add(string));
    }

    /**
     * Helper method to clear all listViews.
     */
    private void clearLists() {
        armyOneStatsListView.getItems().clear();
        armyTwoStatsListView.getItems().clear();
        battleLogListView.getItems().clear();
        battleStatsListView.getItems().clear();
    }

    /**
     * Helper method delete unit from the database.
     * @param armyNumber the army number
     */
    private void deleteUnit(int armyNumber) {
        String message = "Are you sure you want to delete unit: " + selectedUnit.getName() + "?";

        boolean res = ViewUtil.getConfirmation(message);
        if (res) {
            try {
                unitDAO.deleteUnit(selectedUnit);
                armies[armyNumber].getAllUnits().remove(selectedIndex);
                showArmy(armyNumber);
                this.selectedUnit = null;
            } catch (SQLException | IllegalArgumentException e) {
                ViewUtil.giveError(e.getMessage());
            }
        }
    }

    /**
     * Helper method edit unit and save to the database.
     * @param armyNumber the army number
     */
    private void editUnit(int armyNumber) {
        String message = "Are you sure you want to edit unit: " + selectedUnit.getName() + "?";
        boolean res = ViewUtil.getConfirmation(message);
        if (res) {
            UnitSingleton.setUnit(selectedUnit);
            try {
                App.editUnit();
                unitDAO.editUnit(UnitSingleton.getUnit());
                armies[armyNumber].getAllUnits().set(selectedIndex, UnitSingleton.getUnit());
                showArmy(armyNumber);
                this.selectedUnit = null;
            } catch (SQLException | IllegalArgumentException e) {
                ViewUtil.giveError(e.getMessage());
            }
        }
    }

    /**
     * Helper function to create context menu when
     * right-clicking a unit.
     * @param n the army number.
     * @return the context menu.
     */
    private ContextMenu createUnitContextMenu(int n) {
        ContextMenu unitRightClick = new ContextMenu();
        MenuItem editUnit = new MenuItem("Edit Unit");
        MenuItem deleteUnit = new MenuItem("Delete Unit");
        editUnit.setOnAction(event -> editUnit(n));
        deleteUnit.setOnAction(event -> deleteUnit(n));
        deleteUnit.setStyle("-fx-background-color: red;" + "-fx-text-fill: white");
        unitRightClick.getItems().addAll(editUnit, deleteUnit);
        return unitRightClick;
    }

    /**
     * Helper method for handling clicks on unit list views.
     * @param click the mouse event on the list view.
     */
    private void unitListViewClickHandler(MouseEvent click, int armyNumber) {
        if (click.getClickCount() >= 1) {
            Unit selectedUnit = armyUnitsListViews[armyNumber].getSelectionModel().getSelectedItem();
            int selectedIndex = armyUnitsListViews[armyNumber].getSelectionModel().getSelectedIndex();
            if (selectedUnit != null) {
                this.selectedUnit = selectedUnit;
                this.selectedIndex = selectedIndex;
                if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                    editUnit(armyNumber);
                } else if (click.getButton() == MouseButton.SECONDARY && click.getClickCount() == 1) {
                    armyUnitsListViews[armyNumber].setContextMenu(createUnitContextMenu(0));
                }
            }
        }
    }

    /**
     * Helper method to make the combobox of armies look better.
     * @param dropDownNumber the army list dropdown number.
     * @return a StringConverter from the type army, to the type string and vice versa.
     */
    private StringConverter<Army> comboBoxStringConverter(int dropDownNumber) {
        return new StringConverter<Army>() {
            @Override
            public String toString(Army army) {
                if (army != null) return army.getName(); else return "Select Army";
            }

            @Override
            public Army fromString(String string) {
                return chooseArmyComboBoxes[dropDownNumber].getItems().stream().filter(army -> army.getName().equals(string)).findFirst().orElse(null);
            }
        };
    }

    /**
     * Override method from the JavaFX class Initializable.
     * Sets the starting information for the scene to
     * show.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        resetBtn.setDisable(true);
        armies = new Army[2];
        armyStatListViews = new ListView[] { armyOneStatsListView, armyTwoStatsListView };
        armyNameLabels = new Label[] { armyOneNameLabel, armyTwoNameLabel };
        armyUnitsListViews = new ListView[] { armyOneUnitsListView, armyTwoUnitsListView };
        chooseArmyComboBoxes = new ComboBox[] { chooseArmyOneCombo, chooseArmyTwoCombo };

        updateArmyDropSelector();
        chooseArmyOneCombo.setConverter(comboBoxStringConverter(0));
        chooseArmyTwoCombo.setConverter(comboBoxStringConverter(1));

        chooseTerrainCombo.getItems().addAll(TerrainType.values());
        chooseTerrainCombo.setValue(TerrainType.PLAINS);

        // Allows for double-clicking battle log entries to see what happened
        battleLogListView.setOnMouseClicked(click -> {
            if (click.getButton() == MouseButton.PRIMARY && click.getClickCount() == 2) {
                String selectedLogEntry = battleLogListView.getSelectionModel().getSelectedItem();
                if (selectedLogEntry != null) {
                    ViewUtil.giveLogEntry(selectedLogEntry);
                }
            }
        });

        // Methods to delete or edit units and save them to csv.
        armyOneUnitsListView.setOnMouseClicked(click -> {
            unitListViewClickHandler(click, 0);
        });
        armyTwoUnitsListView.setOnMouseClicked(click -> {
            unitListViewClickHandler(click, 1);
        });
    }
}
