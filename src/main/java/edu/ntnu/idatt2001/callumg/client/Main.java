package edu.ntnu.idatt2001.callumg.client;

import edu.ntnu.idatt2001.callumg.model.database.CreateDatabase;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

/**
 * The class main is used for packaging to Jar.
 * Jar files are not able to directly run classes that extend third
 * party libraries.
 * @author Callum Gran
 * @version 1.1 22.05.2022
 */
public class Main {

    /**
     * The entry point of application.
     * Creates database for the program if no database exists.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        if (!Files.exists(Path.of("wargames.db"))) {
            try {
                CreateDatabase.createNewDatabase("wargames.db");
                CreateDatabase.createNewTables("wargames.db");
            } catch (SQLException e) {
                ViewUtil.giveError("Could not create database.");
            }
        }
        App.main(args);
    }
}
