package edu.ntnu.idatt2001.callumg.client.controller;

import edu.ntnu.idatt2001.callumg.client.UnitSingleton;
import edu.ntnu.idatt2001.callumg.client.ViewUtil;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * Controller class for the edit unit stage.
 * This controller handles events and
 * information shown on the editUnit fxml file.
 *
 * @author Callum Gran
 * @version 1.4 28.05.2022
 */
public class EditUnitController implements Initializable {

    @FXML
    private Button cancelBtn, saveBtn;

    @FXML
    private TextField unitArmorInput, unitAttackInput, unitHealthInput, unitNameInput;

    @FXML
    private ComboBox<UnitType> unitTypeComboBox;

    /**
     * Method to close the stage.
     */
    @FXML
    void cancel() {
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        stage.close();
    }

    /**
     * Method to save the unit being edited.
     * This method swaps the unit with a new unit,
     * and then sends this information to UnitSingleton.
     */
    @FXML
    void save() {
        Unit unit = UnitFactory.getUnit(unitTypeComboBox.getValue(),
                unitNameInput.getText(),
                Integer.parseInt(unitHealthInput.getText()),
                Integer.parseInt(unitAttackInput.getText()),
                Integer.parseInt(unitArmorInput.getText())
        );
        unit.setId(UnitSingleton.getUnit().getId());
        UnitSingleton.setUnit(unit);
        ViewUtil.giveInformation("The unit was saved.");
        Stage stage = (Stage) saveBtn.getScene().getWindow();
        stage.close();
    }

    /**
     * Override method from the JavaFX class Initializable.
     * Sets the starting information for the scene to
     * show.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Gets unit information
        Unit unit = UnitSingleton.getUnit();
        unitTypeComboBox.getItems().addAll(UnitType.values());
        //Gets the unit type that the unit being edited has.
        unitTypeComboBox.setValue(Arrays.stream(UnitType.values()).filter(type -> type.getType().equals(unit.getClass().getSimpleName())).toList().get(0));
        //This method is in place to show a more readable format
        //of the unit types, yet still get the correct UnitType.
        unitTypeComboBox.setConverter(
            new StringConverter<>() {
                @Override
                public String toString(UnitType unitType) {
                    return unitType.getType();
                }

                @Override
                public UnitType fromString(String string) {
                    return unitTypeComboBox.getItems().stream().filter(type -> type.getType().equals(string)).findFirst().orElse(null);
                }
            }
        );

        unitNameInput.setText(unit.getName());
        unitHealthInput.setText(Integer.toString(unit.getHealth()));
        unitAttackInput.setText(Integer.toString(unit.getAttack()));
        unitArmorInput.setText(Integer.toString(unit.getArmor()));

        // The following functions make sure that the user cannot input text
        // that would cause errors.
        ViewUtil.addRegexHandler(unitNameInput, "No special characters");
        ViewUtil.addRegexHandler(unitHealthInput, "Only numbers");
        ViewUtil.addTextLimiter(unitHealthInput, 9);
        ViewUtil.addRegexHandler(unitArmorInput, "Only numbers");
        ViewUtil.addTextLimiter(unitArmorInput, 9);
        ViewUtil.addRegexHandler(unitAttackInput, "Only numbers");
        ViewUtil.addTextLimiter(unitAttackInput, 9);
    }
}
