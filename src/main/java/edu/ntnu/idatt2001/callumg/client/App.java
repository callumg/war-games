package edu.ntnu.idatt2001.callumg.client;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * Class that creates stages and scenes for the application.
 * Extends the JavaFX class Application.
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public class App extends Application {

    /**
     * The entry point of application, called upon by the Main class for running from JAR.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Overridden method from Application.
     * This method sets the first stage that opens
     * when you open the application.
     * @param primaryStage the main stage of the application.
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("War Games");
        primaryStage.setScene(new Scene(new AnchorPane()));
        ViewUtil.createStage("warGames", primaryStage);
        primaryStage.show();
    }

    /**
     * Method that opens the editArmy stage
     * from resources.
     */
    public static void editArmy() {
        Stage stage = new Stage();
        stage.setScene(new Scene(new AnchorPane()));
        ViewUtil.createStage("manageArmies", stage);
        stage.showAndWait();
    }

    /**
     * Method that opens the editUnit stage
     * from resources.
     */
    public static void editUnit() {
        Stage stage = new Stage();
        stage.setScene(new Scene(new AnchorPane()));
        ViewUtil.createStage("editUnit", stage);
        stage.showAndWait();
    }
}
