package edu.ntnu.idatt2001.callumg.client.controller;

import edu.ntnu.idatt2001.callumg.client.App;
import edu.ntnu.idatt2001.callumg.client.UnitSingleton;
import edu.ntnu.idatt2001.callumg.client.ViewUtil;
import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.database.ArmyDAO;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.FileHandler;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * Controller class for the edit army stage.
 * This controller handles events and
 * information shown on the editArmy fxml file.
 *
 * @author Callum Gran
 * @version 1.1  28.05.2022
 */
public class EditArmyController implements Initializable {

    private ArmyDAO armyDAO = new ArmyDAO();

    @FXML
    private Label importArmyPath;

    @FXML
    private Button cancelBtn, saveArmyBtn;

    @FXML
    private TextField armyNameInput, unitAmountInput, unitHealthInput, unitNameInput;

    @FXML
    private ListView<Unit> unitListView;

    @FXML
    private ListView<Army> armiesListView;

    @FXML
    private ComboBox<UnitType> unitTypeComboBox;

    private Army selectedArmy = null;
    private Unit selectedUnit = null;
    private int selectedIndex;
    private ObservableList<Army> allArmies;

    /**
     * Method to add units to unitListView.
     * It uses the UnitFactory to create multiple units.
     */
    @FXML
    void addUnit() {
        UnitType unitType = unitTypeComboBox.getValue();
        String unitName = unitNameInput.getText();
        int unitHealth = Integer.parseInt(unitHealthInput.getText());
        int unitAmount = Integer.parseInt(unitAmountInput.getText());
        try {
            UnitFactory.getManyUnits(unitType, unitName, unitHealth, unitAmount).forEach(unit -> unitListView.getItems().add(unit));
        } catch (IllegalArgumentException e) {
            ViewUtil.giveError(e.getMessage());
        }
        clearFields();
    }

    /**
     * Method to import armies.
     * It opens a window for the user to select a file to import.
     */
    @FXML
    void importArmy() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select army to import");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(new Stage());
        try {
            if (file != null) {
                Army importedArmy = FileHandler.readFromFile(file.getPath());
                armiesListView.getItems().add(armyDAO.addArmy(importedArmy));
                importArmyPath.setText(file.getPath());
            }
        } catch (IOException e) {
            ViewUtil.giveError("The file type you chose is not valid");
        } catch (NullPointerException e) {
            ViewUtil.giveError("The file is corrupted");
        } catch (InvalidPathException e) {
            ViewUtil.giveError("There was an error with the file path");
        } catch (SQLException e) {
            ViewUtil.giveError("There was an error in the database");
        } catch (IllegalArgumentException e) {
            ViewUtil.giveError(e.getMessage());
        }
    }

    /**
     * Method to close the stage.
     */
    @FXML
    void cancelArmyCreation() {
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        stage.close();
    }

    /**
     * Method to save an army.
     * This method will either save an army if it already exists in the database,
     * or it will create a new army and save it to the database.
     */
    @FXML
    void saveArmy() {
        if (armyNameInput.getText().isBlank()) {
            ViewUtil.giveError("The army name cannot be empty");
        } else if (unitListView.getItems().size() == 0) {
            ViewUtil.giveError("The army must have troops");
        } else {
            if (selectedArmy != null && selectedArmy.getId() != 0) {
                try {
                    Army editedArmy = new Army(selectedArmy.getId(), createArmyFromFields().getName(), createArmyFromFields().getAllUnits());
                    armyDAO.editArmy(editedArmy);
                    ViewUtil.giveInformation("The army was saved.");
                    Stage stage = (Stage) saveArmyBtn.getScene().getWindow();
                    stage.close();
                } catch (IllegalArgumentException | SQLException e) {
                    ViewUtil.giveError(e.getMessage());
                }
            } else {
                try {
                    armyDAO.addArmy(createArmyFromFields());
                    ViewUtil.giveInformation("The army was saved.");
                    Stage stage = (Stage) saveArmyBtn.getScene().getWindow();
                    stage.close();
                } catch (IllegalArgumentException | SQLException e) {
                    ViewUtil.giveError(e.getMessage());
                }
            }
        }
    }

    /**
     * Method to save the army that has been created.
     * This method creates a new .csv file containing the
     * army information, so it can be imported and used.
     * The method gives errors if the army name is blank
     * or if the army doesn't have any troops.
     */
    @FXML
    void exportToFile() {
        if (armyNameInput.getText().isBlank()) {
            ViewUtil.giveError("The army name cannot be empty");
        } else if (unitListView.getItems().size() == 0) {
            ViewUtil.giveError("The army must have troops");
        } else {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select location to save army");
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
            fileChooser.getExtensionFilters().add(extFilter);
            File file = fileChooser.showSaveDialog(new Stage());
            try {
                FileHandler.exportToFile(createArmyFromFields(), file.getPath());
                ViewUtil.giveInformation("The file was exported.");
            } catch (InvalidPathException e) {
                ViewUtil.giveError("The file directory you gave is invalid.");
            } catch (IllegalArgumentException | IOException e) {
                ViewUtil.giveError(e.getMessage());
            }
        }
    }

    /**
     * Method to delete a selected unit from the database.
     * Gives user a popup to make sure they want to delete the unit.
     */
    @FXML
    void deleteArmy() {
        if (selectedArmy == null) {
            ViewUtil.giveError("There is no army selected. Click on a army to select it.");
        } else {
            boolean res = ViewUtil.getConfirmation("Are you sure you want to delete army: " + selectedArmy.getName() + "?");

            if (res) {
                try {
                    armyDAO.deleteArmy(selectedArmy);
                    armiesListView.getItems().remove(selectedArmy);
                    selectedArmy = null;
                } catch (SQLException | IllegalArgumentException e) {
                    ViewUtil.giveError(e.getMessage());
                }
            }
        }
    }

    /**
     * Method to delete a selected unit from the unitListView.
     * Gives user a popup to make sure they want to delete the unit.
     */
    @FXML
    void deleteSelectedUnit() {
        if (selectedUnit == null) {
            ViewUtil.giveError("There is no unit selected. Click on a unit to select it.");
        } else {
            boolean res = ViewUtil.getConfirmation("Are you sure you want to delete unit: " + selectedUnit.getName() + "?");

            if (res) {
                unitListView.getItems().remove(selectedUnit);
                selectedUnit = null;
            }
        }
    }

    /**
     * Method to edit a selected unit from the unitListView.
     * This method opens the stage editUnit and saves the unit
     * information in UnitSingleton to pass the information.
     * Gives user a popup to make sure they want to edit the unit.
     */
    @FXML
    void editSelectedUnit() {
        if (selectedUnit == null) {
            ViewUtil.giveError("There is no unit selected. Click on a unit to select it.");
        } else {
            boolean res = ViewUtil.getConfirmation("Are you sure you want to edit unit: " + selectedUnit.getName() + "?");

            if (res) {
                UnitSingleton.setUnit(selectedUnit);
                App.editUnit();
                unitListView.getItems().set(selectedIndex, UnitSingleton.getUnit());
                selectedUnit = null;
            }
        }
    }

    /**
     * Method that selects data from an army in the database
     * and allows the user to change its data.
     */
    @FXML
    void editSelectedArmy() {
        if (selectedArmy == null) {
            ViewUtil.giveError("There is no army selected. Click on a unit to select it.");
        } else {
            boolean res = ViewUtil.getConfirmation("Are you sure you want to edit army: " + selectedArmy.getName() + "?");

            if (res) {
                armyNameInput.setText(selectedArmy.getName());
                unitListView.setItems((ObservableList<Unit>) selectedArmy.getAllUnits());
            }
        }
    }

    /**
     * Helper method to create army from fields on the stage.
     *
     * @return new army from fields.
     */
    private Army createArmyFromFields() {
        List<Unit> units = new ArrayList<>();
        unitListView.getItems().forEach(unit -> units.add(unit));
        return new Army(0, armyNameInput.getText(), units);
    }

    /**
     * Helper method to clear all fields in the input.
     * Used after adding units.
     */
    private void clearFields() {
        unitNameInput.clear();
        unitHealthInput.clear();
        unitAmountInput.clear();
    }

    /**
     * Helper method for handling clicks on the unitListView.
     * @param click the mouse event on the list view.
     */
    private void unitListViewClickHandler(MouseEvent click) {
        if (click.getButton() == MouseButton.PRIMARY) {
            Unit selectedUnit = unitListView.getSelectionModel().getSelectedItem();
            int selectedIndex = unitListView.getSelectionModel().getSelectedIndex();
            if (selectedUnit != null) {
                if (click.getClickCount() >= 1) {
                    this.selectedUnit = selectedUnit;
                    this.selectedIndex = selectedIndex;
                    if (click.getClickCount() == 2) {
                        editSelectedUnit();
                    }
                }
            }
        }
    }

    /**
     * Helper method for handling clicks on the armiesListView.
     * @param click the mouse event on the list view.
     */
    private void armiesListViewClickHandler(MouseEvent click) {
        if (click.getButton() == MouseButton.PRIMARY) {
            Army selectedArmy = armiesListView.getSelectionModel().getSelectedItem();
            if (selectedArmy != null) {
                if (click.getClickCount() >= 1) {
                    this.selectedArmy = selectedArmy;
                    if (click.getClickCount() == 2) {
                        editSelectedArmy();
                    }
                }
            }
        }
    }

    /**
     * Override method from the JavaFX class Initializable.
     * Sets the starting information for the scene to
     * show.
     *
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            allArmies = FXCollections.observableList(armyDAO.getArmies());
        } catch (SQLException | IllegalArgumentException e) {
            ViewUtil.giveError(e.getMessage());
        }

        armiesListView.setItems(allArmies);
        unitTypeComboBox.getItems().addAll(UnitType.values());
        unitTypeComboBox.setValue(UnitType.INFANTRY_UNIT);

        //This method is in place to show a more readable format
        //of the unit types, yet still get the correct UnitType.
        unitTypeComboBox.setConverter(
            new StringConverter<>() {
                @Override
                public String toString(UnitType unitType) {
                    return unitType.getType();
                }

                @Override
                public UnitType fromString(String string) {
                    return unitTypeComboBox.getItems().stream().filter(type -> type.getType().equals(string)).findFirst().orElse(null);
                }
            }
        );

        // The following functions make sure that the user cannot input text
        // that would cause errors.
        ViewUtil.addRegexHandler(armyNameInput, "No special characters");
        ViewUtil.addRegexHandler(unitNameInput, "No special characters");
        ViewUtil.addRegexHandler(unitHealthInput, "Only numbers");
        // Text Limiter is used to not make the input larger than the max value for an integer.
        ViewUtil.addTextLimiter(unitHealthInput, 9);

        // Method to select a unit from the listview.
        // This is later used give the singleton the correct unit or if double-clicked it opens the editor.
        unitListView.setOnMouseClicked(click -> {
            unitListViewClickHandler(click);
        });
        // Method to select an army from the listview.
        armiesListView.setOnMouseClicked(click -> {
            armiesListViewClickHandler(click);
        });
    }
}
