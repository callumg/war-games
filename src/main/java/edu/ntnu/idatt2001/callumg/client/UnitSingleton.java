package edu.ntnu.idatt2001.callumg.client;

import edu.ntnu.idatt2001.callumg.model.units.Unit;

/**
 * The Unit Singleton is used to send
 * information about a unit between controllers.
 *
 * @author Callum Gran
 * @version 1.1 22.05.2022
 */
public class UnitSingleton {

    private static Unit unit;

    /**
     * A private constructor to hinder the creation of an instance.
     * @throws IllegalArgumentException
     */
    private UnitSingleton() throws IllegalArgumentException {
        throw new IllegalArgumentException("Cannot instantiate an eager singleton");
    }

    /**
     * Gets the unit.
     *
     * @return the unit to save
     */
    public static Unit getUnit() {
        return unit;
    }

    /**
     * Sets unit.
     *
     * @param unit the unit to be saved
     */
    public static void setUnit(Unit unit) {
        UnitSingleton.unit = unit;
    }
}
