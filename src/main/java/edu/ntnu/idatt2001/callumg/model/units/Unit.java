package edu.ntnu.idatt2001.callumg.model.units;

import java.util.regex.Pattern;

/**
 * Class Unit is an abstract superclass.
 *
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public abstract class Unit {

    private int id;
    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Instantiates a new Unit.
     * This constructor is for data from the database.
     */
    public Unit() {}

    /**
     * Instantiates a new Unit.
     *
     * @param id     the id of the unit from the database.
     * @param name   name of the unit
     * @param health health of the unit
     * @param attack attack of the unit
     * @param armor  armor of the unit
     * @throws IllegalArgumentException thrown if name is empty, name contains special characters, health is 0 or negative, attack is negative or if armor is negative.
     */
    public Unit(int id, String name, int health, int attack, int armor) throws IllegalArgumentException {
        Pattern specialCharacterPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        if (name.isBlank()) throw new IllegalArgumentException("The name of the unit cannot be empty or blank.");
        if (specialCharacterPattern.matcher(name).find()) throw new IllegalArgumentException("The name of the unit cannot contain special characters.");
        if (health <= 0) throw new IllegalArgumentException("The health of the unit cannot be less than or equal to 0 at initiation.");
        if (attack < 0) throw new IllegalArgumentException("The attack stat of the unit cannot be negative.");
        if (armor < 0) throw new IllegalArgumentException("The armor stat of the unit cannot be negative.");
        this.id = id;
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Gets id.
     *
     * @return id of the unit
     */
    public int getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return name of the unit
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health.
     *
     * @return health of the unit
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets attack.
     *
     * @return attack of the unit
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor.
     *
     * @return armor of the unit
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets id.
     *
     * @param id the id of the unit from the db
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the name.
     * @param name the name of the unit.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets attack.
     *
     * @param attack the attack of the unit.
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * Sets armor.
     *
     * @param armor the armor of the unit.
     */
    public void setArmor(int armor) {
        this.armor = armor;
    }

    /**
     * Sets health.
     *
     * @param health new health for the unit
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain the attack is in
     * @return attack bonus of the unit
     */
    public abstract int getAttackBonus(int terrain);

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain the defence is in
     * @return resist bonus of the unit
     */
    public abstract int getResistBonus(int terrain);

    /**
     * Method to deal damage to an opposing unit.
     * Method only deals damage if the total damage is more than the total defence
     * as not to heal the enemy.
     *
     * @param unit    the unit that is getting attacked
     * @param terrain the terrain
     * @return the int
     */
    public int attack(Unit unit, int terrain) {
        int totalAttack = getAttack() + getAttackBonus(terrain);
        int unitTotalDefence = unit.getArmor() + unit.getResistBonus(terrain);
        if (totalAttack > unitTotalDefence) {
            unit.setHealth(unit.getHealth() - (totalAttack - unitTotalDefence));
        }
        return totalAttack - unitTotalDefence;
    }

    /**
     * To string for csv.
     *
     * @return the string to be written to csv.
     */
    public String toStringCsv() {
        return this.getClass().getSimpleName() + "," + this.getName() + "," + this.getHealth();
    }

    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + ": " + getName() + " | " + "HP: " + getHealth() + " | " + "Attack: " + getAttack() + " | " + "Armour: " + getArmor() + '\n');
    }
}
