package edu.ntnu.idatt2001.callumg.model.units.specialized;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;

/**
 * Class RangedUnit inherits from the abstract superclass Unit.
 *
 * @author Callum Gran
 * @version 1.3 22.05.2022
 */
public class RangedUnit extends Unit {

    private static final int RANGED_ATTACK = 15;
    private static final int RANGED_ATTACK_BONUS = 3;
    private static final int RANGED_ARMOR = 8;
    private static final int STARTING_RANGED_RESIST_BONUS = 6;
    private static final int WEAKENED_RANGED_RESIST_BONUS = 4;
    private static final int BASE_RANGED_RESIST_BONUS = 2;
    private int timesAttacked;

    /**
     * Instantiates a new RangedUnit.
     * This constructor is for data from the database.
     */
    public RangedUnit() {
        super();
    }

    /**
     * Instantiates a new RangedUnit.
     * Uses the constructor of its superclass.
     *
     * @param name   name of the unit
     * @param health health of the unit
     * @param attack attack of the unit
     * @param armor  armor of the unit
     */
    public RangedUnit(int id, String name, int health, int attack, int armor) {
        super(id, name, health, attack, armor);
    }

    /**
     * Instantiates a new RangedUnit.
     * Uses the constants defined in the top.
     *
     * @param name   name of the unit
     * @param health health of the unit
     */
    public RangedUnit(int id, String name, int health) {
        this(id, name, health, RANGED_ATTACK, RANGED_ARMOR);
    }

    @Override
    public int getAttackBonus(int terrain) {
        if (terrain == TerrainType.HILL.getCode()) {
            return RANGED_ATTACK_BONUS + 2;
        } else if (terrain == TerrainType.FOREST.getCode()) {
            return RANGED_ATTACK_BONUS - 2;
        }
        return RANGED_ATTACK_BONUS;
    }

    @Override
    public int getResistBonus(int terrain) {
        int resistBonus;
        if (timesAttacked == 0) resistBonus = STARTING_RANGED_RESIST_BONUS; else if (timesAttacked == 1) resistBonus = WEAKENED_RANGED_RESIST_BONUS; else resistBonus = BASE_RANGED_RESIST_BONUS;
        timesAttacked++;
        return resistBonus;
    }
}
