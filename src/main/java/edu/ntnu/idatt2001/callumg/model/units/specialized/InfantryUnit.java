package edu.ntnu.idatt2001.callumg.model.units.specialized;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;

/**
 * Class InfantryUnit inherits from the abstract superclass Unit.
 *
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public class InfantryUnit extends Unit {

    private static final int INFANTRY_ATTACK = 15;
    private static final int INFANTRY_ATTACK_BONUS = 2;
    private static final int INFANTRY_ARMOR = 10;
    private static final int INFANTRY_RESIST_BONUS = 1;

    /**
     * Instantiates a new InfantryUnit.
     * This constructor is for data from the database.
     */
    public InfantryUnit() {
        super();
    }

    /**
     * Instantiates a new InfantryUnit.
     * Uses the constructor of its superclass.
     *
     * @param name   name of the unit
     * @param health health of the unit
     * @param attack attack of the unit
     * @param armor  armor of the unit
     */
    public InfantryUnit(int id, String name, int health, int attack, int armor) {
        super(id, name, health, attack, armor);
    }

    /**
     * Instantiates a new InfantryUnit.
     * Uses the constants defined in the top.
     *
     * @param name   name of the unit
     * @param health health of the unit
     */
    public InfantryUnit(int id, String name, int health) {
        this(id, name, health, INFANTRY_ATTACK, INFANTRY_ARMOR);
    }

    @Override
    public int getAttackBonus(int terrain) {
        if (terrain == TerrainType.PLAINS.getCode()) {
            return INFANTRY_ATTACK_BONUS + 2;
        } else {
            return INFANTRY_ATTACK_BONUS;
        }
    }

    @Override
    public int getResistBonus(int terrain) {
        if (terrain == TerrainType.PLAINS.getCode()) {
            return INFANTRY_RESIST_BONUS + 2;
        } else {
            return INFANTRY_RESIST_BONUS;
        }
    }
}
