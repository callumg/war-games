package edu.ntnu.idatt2001.callumg.model.database;

import static edu.ntnu.idatt2001.callumg.model.database.Database.close;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The Army Database Access Object.
 * This class is to send data from armies to and from the database.
 * @author Callum Gran
 * @version 1.1 28.05.2022
 */
public class ArmyDAO {

    private UnitDAO unitDAO = new UnitDAO();

    /**
     * Gets all armies in the database.
     *
     * @return all saved armies
     * @throws SQLException             sql errors.
     */
    public List<Army> getArmies() throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Army> armies = new ArrayList<>();
        String sql = "SELECT * FROM armies";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Army army = new Army();
                army.setId(resultSet.getInt("army_id"));
                army.setName(resultSet.getString("army_name"));
                army.addAllUnits(unitDAO.getUnits(resultSet.getInt("army_id")));
                armies.add(army);
            }
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return armies;
    }

    /**
     * Method to add an army and all its units to the database and gives the army an id.
     *
     * @param army the army to add
     * @return the army that was added
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is not 1, meaning it already is in the database.
     */
    public Army addArmy(Army army) throws SQLException, IllegalArgumentException {
        if (army.getId() != 0) throw new IllegalArgumentException("This unit already exists in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String sql = "INSERT INTO armies (army_name) VALUES (?)";

        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setString(1, army.getName());
            int result = preparedStatement.executeUpdate();
            if (result == 1) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) army.setId(resultSet.getInt(1));
            }
            connection.close();
            for (Unit unit : army.getAllUnits()) {
                unitDAO.addUnit(unit, army.getId());
            }
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return army;
    }

    /**
     * Method to edit an army's data in the database.
     *
     * @param army the army to edit
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is 0, meaning it is not in the database.
     */
    public void editArmy(Army army) throws SQLException, IllegalArgumentException {
        if (army.getId() == 0) throw new IllegalArgumentException("This army is not in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql = "UPDATE armies SET army_name = ? WHERE army_id = ?";
        String unitDeleteSql = "DELETE FROM units WHERE army_id = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, army.getName());
            preparedStatement.setInt(2, army.getId());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(unitDeleteSql);
            preparedStatement.setInt(1, army.getId());
            preparedStatement.executeUpdate();
            connection.close();
            for (Unit unit : army.getAllUnits()) {
                unit.setId(0);
                unitDAO.addUnit(unit, army.getId());
            }
        } finally {
            close(connection, preparedStatement, null);
        }
    }

    /**
     * Method to delete an army from the database.
     *
     * @param army the army to delete
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is 0, meaning it is not in the database.
     */
    public void deleteArmy(Army army) throws SQLException, IllegalArgumentException {
        if (army.getId() == 0) throw new IllegalArgumentException("This army is not in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM armies WHERE army_id = ?";
        String unitSql = "DELETE FROM units WHERE army_id = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, army.getId());
            preparedStatement.executeUpdate();
            preparedStatement = connection.prepareStatement(unitSql);
            preparedStatement.setInt(1, army.getId());
            preparedStatement.executeUpdate();
        } finally {
            close(connection, preparedStatement, null);
        }
    }
}
