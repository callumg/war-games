package edu.ntnu.idatt2001.callumg.model.util;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import java.util.ArrayList;

/**
 * The type Unit factory.
 *
 * @author Callum Gran
 * @version 1.3 22.05.2022
 */
public class UnitFactory {

    private UnitFactory() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Cannot create object from utility class.");
    }

    /**
     * Method that gets a unit.
     *
     * @param unitType the unit type
     * @param name     the name
     * @param health   the health
     * @return the unit
     * @throws IllegalArgumentException the illegal argument exception
     */
    //I use if statements instead of a switch case, as it has been shown to have
    //better performance.
    public static Unit getUnit(UnitType unitType, String name, int health, int attack, int armor) throws IllegalArgumentException {
        Unit unit;
        if (unitType.getCode() == UnitType.INFANTRY_UNIT.getCode()) unit = new InfantryUnit(0, name, health, attack, armor);
        else if (unitType.getCode() == UnitType.RANGED_UNIT.getCode()) unit = new RangedUnit(0, name, health, attack, armor);
        else if (unitType.getCode() == UnitType.CAVALRY_UNIT.getCode()) unit = new CavalryUnit(0, name, health, attack, armor);
        else if (unitType.getCode() == UnitType.COMMANDER_UNIT.getCode()) unit = new CommanderUnit(0, name, health, attack, armor);
            // This exception is redundant as it will never be thrown.
        else throw new IllegalArgumentException("This unit type doesn't exist");
        return unit;
    }


    /**
     * Method that gets a unit.
     *
     * @param unitType the unit type
     * @param name     the name
     * @param health   the health
     * @return the unit
     * @throws IllegalArgumentException the illegal argument exception
     */
    //I use if statements instead of a switch case, as it has been shown to have
    //better performance.
    public static Unit getUnit(UnitType unitType, String name, int health) throws IllegalArgumentException {
        Unit unit;
        if (unitType.getCode() == UnitType.INFANTRY_UNIT.getCode()) unit = new InfantryUnit(0, name, health);
        else if (unitType.getCode() == UnitType.RANGED_UNIT.getCode()) unit = new RangedUnit(0, name, health);
        else if (unitType.getCode() == UnitType.CAVALRY_UNIT.getCode()) unit = new CavalryUnit(0, name, health);
        else if (unitType.getCode() == UnitType.COMMANDER_UNIT.getCode()) unit = new CommanderUnit(0, name, health);
        // This exception is redundant as it will never be thrown.
        else throw new IllegalArgumentException("This unit type doesn't exist");
        return unit;
    }

    /**
     * Method that gets many units.
     *
     * @param unitType the unit type
     * @param name     the name
     * @param health   the health
     * @param amount   the amount
     * @return the many units
     * @throws IllegalArgumentException the illegal argument exception
     */
    public static ArrayList<Unit> getManyUnits(UnitType unitType, String name, int health, int amount) throws IllegalArgumentException {
        if (amount <= 0) throw new IllegalArgumentException("You cannot create 0 or less troops.");
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i < amount; i++) units.add(getUnit(unitType, name, health));
        return units;
    }
}
