package edu.ntnu.idatt2001.callumg.model.util;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import java.io.*;
import java.nio.file.InvalidPathException;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * The file handler to import and export data to files.
 *
 * @author Callum Gran
 * @version 1.2  29.05.2022
 */
public class FileHandler {

    private FileHandler() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Cannot create object from utility class.");
    }

    /**
     * Export to file.
     * This method exports all data of an army, so it can be imported at a later date.
     *
     * @param army the army to be exported.
     * @param file the file to receive the data.
     * @throws IllegalArgumentException the illegal argument exception
     * @throws IOException              the io exception
     * @throws InvalidPathException     the invalid path exception
     */
    public static void exportToFile(Army army, String file) throws IllegalArgumentException, IOException, InvalidPathException {
        if (army == null) throw new IllegalArgumentException("Army cannot be null.");
        if (!(file.endsWith(".csv"))) throw new InvalidPathException(file, "File must be of type CSV.");
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file))) {
            bufferedWriter.write(army.getName() + '\n');
            for (Unit unit : army.getAllUnits()) {
                bufferedWriter.write(unit.toStringCsv() + '\n');
            }
        }
    }

    /**
     * Read from file army.
     * This method imports all data of an army from a file.
     *
     * @param file the file to import from.
     * @return the imported army.
     * @throws IOException          the io exception
     * @throws InvalidPathException the invalid path exception
     */
    public static Army readFromFile(String file) throws IOException, InvalidPathException {
        if (!(file.endsWith(".csv"))) throw new InvalidPathException(file, "File must be of type CSV.");
        Army army;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line = bufferedReader.readLine();
            if (line.isBlank()) throw new IOException("Army name in file is blank.");
            if (Pattern.compile("[^a-zA-Z0-9]").matcher(line).find()) throw new IOException("Army name in file contains special characters");
            army = new Army(0, line);
            while ((line = bufferedReader.readLine()) != null) {
                String[] unitData = line.split(",");
                if (unitData.length == 3) {
                    UnitType unitType = Arrays.asList(UnitType.values()).stream().filter(type -> type.getType().equals(unitData[0])).findFirst().orElse(null);
                    if (unitType != null) army.addUnit(UnitFactory.getUnit(unitType, unitData[1], Integer.parseInt(unitData[2])));
                    else throw new IOException("One of the units had a unit type that doesn't exist.");
                } else {
                    throw new IOException("One or more lines in the file were corrupted.");
                }
            }
        }
        return army;
    }
}
