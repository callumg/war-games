package edu.ntnu.idatt2001.callumg.model.units.specialized;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;

/**
 * Class CavalryUnit inherits from the abstract superclass Unit.
 *
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public class CavalryUnit extends Unit {

    private static final int CAVALRY_ATTACK = 20;
    private static final int CAVALRY_ATTACK_BONUS = 2;
    private static final int CHARGED_ATTACK_BONUS = CAVALRY_ATTACK_BONUS + 4;
    private static final int CAVALRY_ARMOR = 12;
    private int timesAttacked;
    private static final int CAVALRY_RESIST_BONUS = 1;

    /**
     * Instantiates a new CavalryUnit.
     * This constructor is for data from the database.
     */
    public CavalryUnit() {
        super();
    }

    /**
     * Instantiates a new CavalryUnit.
     * Uses the constructor of its superclass.
     *
     * @param name   name of the unit
     * @param health health of the unit
     * @param attack attack of the unit
     * @param armor  armor of the unit
     */
    public CavalryUnit(int id, String name, int health, int attack, int armor) {
        super(id, name, health, attack, armor);
    }

    /**
     * Instantiates a new CavalryUnit.
     * Uses the constants defined in the top.
     *
     * @param name   name of the unit
     * @param health health of the unit
     */
    public CavalryUnit(int id, String name, int health) {
        this(id, name, health, CAVALRY_ATTACK, CAVALRY_ARMOR);
    }

    @Override
    public int getAttackBonus(int terrain) {
        if (terrain == 2) {
            int attackBonus;
            if (timesAttacked == 0) attackBonus = CHARGED_ATTACK_BONUS; else attackBonus = CAVALRY_ATTACK_BONUS;
            timesAttacked++;
            return attackBonus + 2;
        } else {
            int attackBonus;
            if (timesAttacked == 0) attackBonus = CHARGED_ATTACK_BONUS; else attackBonus = CAVALRY_ATTACK_BONUS;
            timesAttacked++;
            return attackBonus;
        }
    }

    @Override
    public int getResistBonus(int terrain) {
        if (terrain == TerrainType.FOREST.getCode()) {
            return 0;
        } else {
            return CAVALRY_RESIST_BONUS;
        }
    }
}
