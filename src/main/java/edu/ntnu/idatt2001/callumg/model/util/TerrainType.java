package edu.ntnu.idatt2001.callumg.model.util;

/**
 * The enum Terrain type.
 * @author Callum Gran
 * @version 1.1 22.05.2022
 */
public enum TerrainType {
    /**
     * Hill terrain type.
     */
    HILL(1),
    /**
     * Plains terrain type.
     */
    PLAINS(2),
    /**
     * Forest terrain type.
     */
    FOREST(3);

    private int code;

    TerrainType(int code) {
        this.code = code;
    }

    /**
     * Gets the terrain type code.
     *
     * @return the code
     */
    public int getCode() {
        return code;
    }
}
