package edu.ntnu.idatt2001.callumg.model.util;

/**
 * Enum for showing unit types.
 * @author Callum Gran
 * @version 1.2 22.05.2022
 */
public enum UnitType {
    /**
     * Infantry unit type.
     */
    INFANTRY_UNIT(1, "InfantryUnit"),
    /**
     * Ranged unit type.
     */
    RANGED_UNIT(2, "RangedUnit"),
    /**
     * Cavalry unit type.
     */
    CAVALRY_UNIT(3, "CavalryUnit"),
    /**
     * Commander unit type.
     */
    COMMANDER_UNIT(4, "CommanderUnit");

    private final int code;
    private final String type;

    UnitType(int code, String type) {
        this.code = code;
        this.type = type;
    }

    /**
     * Gets unit type code.
     *
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * Gets unit type as a readable string.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }
}
