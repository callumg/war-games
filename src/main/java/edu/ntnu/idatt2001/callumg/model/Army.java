package edu.ntnu.idatt2001.callumg.model;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;
import javafx.collections.FXCollections;

/**
 * Class Army.
 * An army object stores units and has methods for fighting other armies.
 *
 * @author Callum Gran
 * @version 1.2 28.5.2022
 */
public class Army {

    private int id;
    private String name;
    private List<Unit> unitList;

    /**
     * Instantiates a new Unit.
     * This constructor is for data from the database.
     */
    public Army() {
        unitList = FXCollections.observableList(new ArrayList<>());
    }

    /**
     * Instantiates a new Army.
     * Used if there already is a list of units.
     *
     * @param id       the id
     * @param name     name of the army
     * @param unitList list of units in the army
     * @throws IllegalArgumentException thrown if the name is not blank or empty
     */
    public Army(int id, String name, List<Unit> unitList) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("The name of the army cannot be blank.");
        this.id = id;
        this.name = name;
        this.unitList = FXCollections.observableList(unitList);
    }

    /**
     * Instantiates a new Army.
     * Second constructor if there is no predefined list given.
     *
     * @param id   the id
     * @param name name of the army
     * @throws IllegalArgumentException thrown if the name is not blank or empty
     */
    public Army(int id, String name) throws IllegalArgumentException {
        this(id, name, new ArrayList<>());
    }

    /**
     * Gets name.
     *
     * @return the name of the army
     */
    public String getName() {
        return name;
    }

    /**
     * Gets id.
     *
     * @return the id of the army
     */
    public int getId() {
        return id;
    }

    /**
     * Adds unit and returns a boolean if it was accepted.
     *
     * @param unit the unit to be added
     * @return the boolean if the unit was added or not
     */
    public boolean addUnit(Unit unit) {
        return this.unitList.add(unit);
    }

    /**
     * Add all units in a list and returns a boolean if it was accepted.
     *
     * @param unitList the list of units to be added
     * @return the boolean if the list was added or not
     */
    public boolean addAllUnits(List<Unit> unitList) {
        return this.unitList.addAll(unitList);
    }

    /**
     * Removes a unit and returns a boolean if it was accepted.
     *
     * @param unit the unit to be removed
     * @return the boolean if the unit was removed or not
     */
    public boolean removeUnit(Unit unit) {
        return this.unitList.remove(unit);
    }

    /**
     * Boolean that represents if the army has units.
     *
     * @return the boolean if the army has units or not
     */
    public boolean hasUnits() {
        return !this.unitList.isEmpty();
    }

    /**
     * Gets all infantry units in the Army.
     *
     * @return the infantry units
     */
    public List<Unit> getInfantryUnits() {
        return unitList.stream().filter(unit -> unit instanceof InfantryUnit).collect(Collectors.toList());
    }

    /**
     * Gets all cavalry units in the Army.
     *
     * @return the cavalry units
     */
    public List<Unit> getCavalryUnits() {
        return unitList.stream().filter(unit -> unit instanceof CavalryUnit && !(unit instanceof CommanderUnit)).collect(Collectors.toList());
    }

    /**
     * Gets all ranged units in the Army.
     *
     * @return the ranged units
     */
    public List<Unit> getRangedUnits() {
        return unitList.stream().filter(unit -> unit instanceof RangedUnit).collect(Collectors.toList());
    }

    /**
     * Gets all commander units in the Army.
     *
     * @return the commander units
     */
    public List<Unit> getCommanderUnits() {
        return unitList.stream().filter(unit -> unit instanceof CommanderUnit).collect(Collectors.toList());
    }

    /**
     * Gets all units.
     *
     * @return a List with all the units.
     */
    public List<Unit> getAllUnits() {
        return this.unitList;
    }

    /**
     * Gets random unit from the list of units.
     *
     * @return the random unit that has been fetched or null if there are none.
     */
    public Unit getRandom() {
        if (hasUnits()) {
            Random random = new Random();
            int randInt = random.nextInt(unitList.size());
            return unitList.get(randInt);
        }
        return null;
    }

    /**
     * Army unit info list.
     *
     * @return the list
     */
    public List<String> getArmyUnitInfo() {
        List<String> armyInfo = new ArrayList<>();
        armyInfo.add("Total Units: " + getAllUnits().size());
        armyInfo.add("Infantry Units: " + getInfantryUnits().size());
        armyInfo.add("Ranged Units: " + getRangedUnits().size());
        armyInfo.add("Cavalry Units: " + getCavalryUnits().size());
        armyInfo.add("Commander Units: " + getCommanderUnits().size());
        return FXCollections.observableList(armyInfo);
    }

    /**
     * Sets the army name.
     *
     * @param name the name of the army.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the army id.
     *
     * @param id the id of the army.
     */
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Army: " + name + " with " + unitList.size() + " units";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army army)) return false;
        return Objects.equals(name, army.name) && Objects.equals(unitList, army.unitList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, unitList);
    }
}
