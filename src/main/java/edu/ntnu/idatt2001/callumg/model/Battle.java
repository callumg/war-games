package edu.ntnu.idatt2001.callumg.model;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class Battle simulates battle between two armies.
 *
 * @author Callum Gran
 * @version 1.2  29.05.2022
 */
public class Battle {

    private final Army armyOne;
    private final Army armyTwo;
    private Army winner;
    private final int terrain;
    private final List<String> battleLog;
    private final List<String> battleStats;

    /**
     * Instantiates a new Battle.
     *
     * @param armyOne     army one.
     * @param armyTwo     army two.
     * @param terrainType the terrain type
     * @throws IllegalArgumentException thrown if one or more armies have no units
     * @throws NullPointerException     the null pointer exception
     */
    public Battle(Army armyOne, Army armyTwo, TerrainType terrainType) throws IllegalArgumentException, NullPointerException {
        if (armyOne == null || armyTwo == null) throw new NullPointerException("Both armies must exist");
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("An army cannot be empty.");
        if (armyOne.equals(armyTwo)) throw new IllegalArgumentException("Fighting armies cannot be the same.");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrainType.getCode();
        this.battleLog = FXCollections.observableList(new ArrayList<>());
        this.battleStats = FXCollections.observableList(new ArrayList<>());
    }

    /**
     * Simulate gets the winner in a battle between two armies.
     * The attacker is chosen at random and the battle continues
     * until one army no longer has any more units. The units are
     * selected at random from the according List of units.
     *
     * @return the winner of the battle.
     */
    public Army simulate() {
        Unit attacker;
        Unit defender;
        Army attackerArmy;
        Army defenderArmy;
        int totalDeaths = armyOne.getAllUnits().size() + armyTwo.getAllUnits().size();
        int infantryDeaths = armyOne.getInfantryUnits().size() + armyTwo.getInfantryUnits().size();
        int rangedDeaths = armyOne.getRangedUnits().size() + armyTwo.getRangedUnits().size();
        int cavalryDeaths = armyOne.getCavalryUnits().size() + armyTwo.getCavalryUnits().size();
        int commanderDeaths = armyOne.getCommanderUnits().size() + armyTwo.getCommanderUnits().size();

        Random rd = new Random();

        while (armyOne.hasUnits() && armyTwo.hasUnits()) {
            boolean attack = rd.nextBoolean();

            if (attack) {
                attackerArmy = armyOne;
                defenderArmy = armyTwo;
            } else {
                attackerArmy = armyTwo;
                defenderArmy = armyOne;
            }

            attacker = attackerArmy.getRandom();
            defender = defenderArmy.getRandom();

            int damage = attacker.attack(defender, terrain);

            addAttackLog(attackerArmy, attacker, defenderArmy, defender, damage);

            if (defender.getHealth() <= 0) {
                defenderArmy.removeUnit(defender);
                deathLog(defenderArmy, defender);
            }
        }

        if (armyOne.hasUnits()) winner = armyOne; else winner = armyTwo;

        totalDeaths = totalDeaths - armyOne.getAllUnits().size() - armyTwo.getAllUnits().size();
        infantryDeaths = infantryDeaths - armyOne.getInfantryUnits().size() - armyTwo.getInfantryUnits().size();
        rangedDeaths = rangedDeaths - armyOne.getRangedUnits().size() - armyTwo.getRangedUnits().size();
        cavalryDeaths = cavalryDeaths - armyOne.getCavalryUnits().size() - armyTwo.getCavalryUnits().size();
        commanderDeaths = commanderDeaths - armyOne.getCommanderUnits().size() - armyTwo.getCommanderUnits().size();

        addBattleStats(totalDeaths, infantryDeaths, rangedDeaths, cavalryDeaths, commanderDeaths);

        battleLog.add(winner.getName() + " won!");

        return winner;
    }

    /**
     * Gets battle log.
     *
     * @return the battle log
     */
    public ObservableList<String> getBattleLog() {
        return FXCollections.observableList(battleLog);
    }

    /**
     * Gets battle stats.
     *
     * @return the battle stats
     */
    public ObservableList<String> getBattleStats() {
        return FXCollections.observableList(battleStats);
    }

    /**
     * Helper method to add battlestats.
     * @param totalDeaths total deaths of all units.
     * @param infantryDeaths total deaths of infantry units.
     * @param rangedDeaths total deaths of ranged units.
     * @param cavalryDeaths total deaths of cavalry units.
     * @param commanderDeaths total deaths of commander units.
     */
    private void addBattleStats(int totalDeaths, int infantryDeaths, int rangedDeaths, int cavalryDeaths, int commanderDeaths) {
        battleStats.add("Total Deaths: " + totalDeaths);
        battleStats.add("Infantry Deaths: " + infantryDeaths);
        battleStats.add("Ranged Deaths: " + rangedDeaths);
        battleStats.add("Cavalry Deaths: " + cavalryDeaths);
        battleStats.add("Commander Deaths: " + commanderDeaths);
    }

    /**
     * Helper method to add an attack to the battle log
     * @param attackerArmy the attacking army
     * @param attacker the attacking unit
     * @param defenderArmy the defending army
     * @param defender the defending unit
     * @param damage the damage dealt
     */
    private void addAttackLog(Army attackerArmy, Unit attacker, Army defenderArmy, Unit defender, int damage) {
        battleLog.add(
            attackerArmy.getName() +
            "'s " +
            attacker.getName() +
            " (" +
            attacker.getClass().getSimpleName() +
            ") attacked " +
            defenderArmy.getName() +
            "'s " +
            defender.getName() +
            " (" +
            defender.getClass().getSimpleName() +
            ") for " +
            damage +
            "hp"
        );
    }

    /**
     * Helper method for adding a death to the battle log.
     * @param defenderArmy the defending army
     * @param defender the unit that died
     */
    private void deathLog(Army defenderArmy, Unit defender) {
        battleLog.add(defenderArmy.getName() + "'s " + defender.getName() + " (" + defender.getClass().getSimpleName() + ") was killed");
    }

    @Override
    public String toString() {
        return "Battle:" + '\n' + "Army 1:" + armyOne + '\n' + "Army 2:" + armyTwo + '\n' + "Winner:" + winner;
    }
}
