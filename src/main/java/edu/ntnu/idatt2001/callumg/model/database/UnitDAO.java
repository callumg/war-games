package edu.ntnu.idatt2001.callumg.model.database;

import static edu.ntnu.idatt2001.callumg.model.database.Database.close;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Unit Database Access Object.
 * This class is to send data from units to and from the database.
 * @author Callum Gran
 * @version 1.1 28.05.2022
 */
public class UnitDAO {

    /**
     * Gets all units in an army.
     *
     * @param armyId the id of the army to get units for.
     * @return the units that belong to an army
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if army id is 0, meaning it is not in the database.
     */
    public List<Unit> getUnits(int armyId) throws SQLException, IllegalArgumentException {
        // As the army_id is the primary key of army, it will always be equal or greater than 1.
        if (armyId == 0) throw new IllegalArgumentException("The id received is 0, this army is not in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Unit> units = new ArrayList<>();
        String sql = "SELECT * FROM units WHERE army_id = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, armyId);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Unit unit;
                if (resultSet.getString("unit_type").equals(UnitType.INFANTRY_UNIT.getType())) unit = new InfantryUnit(); else if (
                    resultSet.getString("unit_type").equals(UnitType.RANGED_UNIT.getType())
                ) unit = new RangedUnit(); else if (resultSet.getString("unit_type").equals(UnitType.CAVALRY_UNIT.getType())) unit = new CavalryUnit(); else if (
                    resultSet.getString("unit_type").equals(UnitType.COMMANDER_UNIT.getType())
                ) unit = new CommanderUnit(); else throw new SQLException("UnitType corrupted in the database");
                unit.setId(resultSet.getInt("unit_id"));
                unit.setName(resultSet.getString("unit_name"));
                unit.setHealth(resultSet.getInt("unit_health"));
                unit.setAttack(resultSet.getInt("unit_attack"));
                unit.setArmor(resultSet.getInt("unit_armor"));
                units.add(unit);
            }
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return units;
    }

    /**
     * Method to add a unit to the database and gives the unit an id.
     *
     * @param unit    the unit to add
     * @param army_id the id of the army to add to
     * @return the unit that was added
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is not 1, meaning it already is in the database.
     */
    public Unit addUnit(Unit unit, int army_id) throws SQLException, IllegalArgumentException {
        if (unit.getId() != 0) throw new IllegalArgumentException("This unit already exists in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        UnitType unitType = Arrays.stream(UnitType.values()).filter(type -> type.getType().equals(unit.getClass().getSimpleName())).toList().get(0);
        String sql = "INSERT INTO units (army_id, unit_name, unit_health, unit_attack, unit_armor, unit_type) VALUES (?, ?, ?, ?, ?, ?)";

        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            preparedStatement.setInt(1, army_id);
            preparedStatement.setString(2, unit.getName());
            preparedStatement.setInt(3, unit.getHealth());
            preparedStatement.setInt(4, unit.getAttack());
            preparedStatement.setInt(5, unit.getArmor());
            preparedStatement.setString(6, unitType.getType());
            int result = preparedStatement.executeUpdate();
            if (result == 1) {
                resultSet = preparedStatement.getGeneratedKeys();
                if (resultSet.next()) unit.setId(resultSet.getInt(1));
            }
        } finally {
            close(connection, preparedStatement, resultSet);
        }
        return unit;
    }

    /**
     * Method to edit a unit's data in the database.
     *
     * @param unit the unit to edit
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is 0, meaning it is not in the database.
     */
    public void editUnit(Unit unit) throws SQLException, IllegalArgumentException {
        if (unit.getId() == 0) throw new IllegalArgumentException("This unit is not in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        UnitType unitType = Arrays.stream(UnitType.values()).filter(type -> type.getType().equals(unit.getClass().getSimpleName())).toList().get(0);
        String sql = "UPDATE units SET unit_name = ?, unit_health = ?, unit_attack = ?, unit_armor = ?, unit_type = ? WHERE unit_id = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, unit.getName());
            preparedStatement.setInt(2, unit.getHealth());
            preparedStatement.setInt(3, unit.getAttack());
            preparedStatement.setInt(4, unit.getArmor());
            preparedStatement.setString(5, unitType.getType());
            preparedStatement.setInt(6, unit.getId());
            preparedStatement.executeUpdate();
        } finally {
            close(connection, preparedStatement, null);
        }
    }

    /**
     * Method to delete a unit from the database.
     *
     * @param unit the unit to delete
     * @throws SQLException             sql errors.
     * @throws IllegalArgumentException thrown if unit id is 0, meaning it is not in the database.
     */
    public void deleteUnit(Unit unit) throws SQLException, IllegalArgumentException {
        if (unit.getId() == 0) throw new IllegalArgumentException("This unit is not in the database.");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sql = "DELETE FROM units WHERE unit_id = ?";
        try {
            connection = Database.instance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, unit.getId());
            preparedStatement.executeUpdate();
        } finally {
            close(connection, preparedStatement, null);
        }
    }
}
