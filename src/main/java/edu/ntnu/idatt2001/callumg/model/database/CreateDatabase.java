package edu.ntnu.idatt2001.callumg.model.database;

import java.sql.*;

/**
 * Class to create a new database with tables.
 * This file is semi-copied from the project done in IDATT-1002.
 * @author Callum Gran
 * @version 0.1
 */
public class CreateDatabase {

    /**
     * Connect to and create a new database
     *
     * @param fileName the database file name
     */
    public static void createNewDatabase(String fileName) throws SQLException {
        DriverManager.getConnection("jdbc:sqlite:" + fileName);
    }

    /**
     * Create new database tables.
     *
     * @param fileName the database file name
     */
    public static void createNewTables(String fileName) throws SQLException {
        String url = "jdbc:sqlite:" + fileName;

        String armySql = "CREATE TABLE IF NOT EXISTS armies (\n"
                        + "	army_id integer PRIMARY KEY,\n"
                        + "	army_name text\n"
                        + ");";

        String unitSql =
            "CREATE TABLE IF NOT EXISTS units (\n" +
            " unit_id integer PRIMARY KEY,\n" +
            " army_id integer,\n" +
            " unit_name text,\n" +
            " unit_health int,\n" +
            " unit_attack int,\n" +
            " unit_armor int,\n" +
            " unit_type text,\n" +
            " FOREIGN KEY (army_id) REFERENCES armies(army_id)\n" +
            ");";

        try (Connection conn = DriverManager.getConnection(url); Statement stmt = conn.createStatement()) {
            stmt.execute("PRAGMA foreign_keys = ON");
            stmt.execute(armySql);
            stmt.execute(unitSql);
        }
    }
}
