package edu.ntnu.idatt2001.callumg.model.units.specialized;

/**
 * Class CommanderUnit inherits from superclass CavalryUnit.
 *
 * @author Callum Gran
 * @version 1.1 23.05.2022
 */
public class CommanderUnit extends CavalryUnit {

    private static final int COMMANDER_ATTACK = 25;
    private static final int COMMANDER_ARMOR = 15;

    /**
     * Instantiates a new CommanderUnit.
     * This constructor is for data from the database.
     */
    public CommanderUnit() {
        super();
    }

    /**
     * Instantiates a new CommanderUnit.
     * Uses the constructor of its superclass.
     *
     * @param name   name of the unit
     * @param health health of the unit
     * @param attack attack of the unit
     * @param armor  armor of the unit
     */
    public CommanderUnit(int id, String name, int health, int attack, int armor) {
        super(id, name, health, attack, armor);
    }

    /**
     * Instantiates a new Cavalry unit.
     * Uses the constants defined in the top.
     *
     * @param name   name of the unit
     * @param health health of the unit
     */
    public CommanderUnit(int id, String name, int health) {
        super(id, name, health, COMMANDER_ATTACK, COMMANDER_ARMOR);
    }
}
