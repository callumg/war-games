/**
 * File that contains all modules.
 *
 * @author Callum Gran
 * @version 1.2
 */
module edu.ntnu.idatt2001.callumg {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;

    opens edu.ntnu.idatt2001.callumg.client to javafx.fxml;
    opens edu.ntnu.idatt2001.callumg.client.controller to javafx.fxml;
    exports edu.ntnu.idatt2001.callumg.client;
    exports edu.ntnu.idatt2001.callumg.model;
    exports edu.ntnu.idatt2001.callumg.client.controller;
    exports edu.ntnu.idatt2001.callumg.model.util;
    exports edu.ntnu.idatt2001.callumg.model.database;
    exports edu.ntnu.idatt2001.callumg.model.units;
    exports edu.ntnu.idatt2001.callumg.model.units.specialized;
}
