package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.database.ArmyDAO;
import edu.ntnu.idatt2001.callumg.model.database.Database;
import edu.ntnu.idatt2001.callumg.model.database.UnitDAO;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ArmyDAOTest {

    // All units in the test database have the stats name: "Infantry", health: 10, attack: 10, armor: 10.
    Army armyWithId = new Army(1, "Great Army");
    Army armyWithOutId = new Army(0, "Sublime Army");
    private ArmyDAO armyDAO;

    @BeforeEach
    void setup() {
        armyDAO = new ArmyDAO();
        armyWithOutId.setId(0);
        try {
            CreateTestDB.setUp();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nested
    class GetArmies {

        @Test
        void testGetArmiesReturnsCorrectAmount() {
            try {
                List<Army> armies = armyDAO.getArmies();
                assertEquals(4, armies.size());
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testGetArmiesReturnsCorrectArmies() {
            try {
                List<Army> armies = armyDAO.getArmies();
                assertEquals("Army One", armies.get(0).getName());
                assertEquals("Army Two", armies.get(1).getName());
                assertEquals("Army Three", armies.get(2).getName());
                assertEquals("Army Four", armies.get(3).getName());
            } catch (SQLException e) {
                fail();
            }
        }
    }

    @Nested
    class AddArmy {

        @Test
        void testAddArmyGivesArmyTheCorrectIdAfterAddingItToTheDatabase() {
            try {
                armyDAO.addArmy(armyWithOutId);
                Connection connection = Database.instance().getConnection();
                // This code counts the amount of rows and
                // checks it matches the new id as it the latest id always matches row unless an earlier id is deleted.
                int lastArmyId = connection.prepareStatement("SELECT count(army_id) FROM armies;").executeQuery().getInt(1);
                connection.close();
                assertEquals(lastArmyId, armyWithOutId.getId());
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testAddArmyAddsTheCorrectArmyToTheDatabaseWithTheCorrectUnits() {
            List<Unit> unitList = new ArrayList<>();
            UnitDAO unitDAO = new UnitDAO();
            unitList.addAll(UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Bob", 10, 10));
            Army armyWithOutIdWithUnits = new Army(0, "Unit Army", unitList);
            try {
                armyDAO.addArmy(armyWithOutIdWithUnits);
                Connection connection = Database.instance().getConnection();
                int lastRowId = connection.prepareStatement("SELECT count(army_id) FROM armies;").executeQuery().getInt(1);
                // I know this is bad practice using + in strings and not using prepareStatement.setInt, but it is not necessary as
                // this is a test database and this code isn't interfaced by the user.
                ResultSet armyResultSet = connection.prepareStatement("SELECT * FROM armies WHERE army_id = " + lastRowId).executeQuery();
                assertEquals(armyWithOutIdWithUnits.getName(), armyResultSet.getString("army_name"));
                connection.close();
                List<Unit> unitsFromDB = unitDAO.getUnits(lastRowId);
                // This army has unique unit names compared to the other armies in the db, therefore I only check unit name.
                for (int i = 0; i < armyWithOutIdWithUnits.getAllUnits().size(); i++) {
                    assertEquals(armyWithOutIdWithUnits.getAllUnits().get(i).getName(), unitsFromDB.get(i).getName());
                }
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testAddArmyThrowsExceptionIfIdAlreadyExists() {
            assertThrows(IllegalArgumentException.class, () -> armyDAO.addArmy(armyWithId));
        }
    }

    @Nested
    class EditArmy {

        @Test
        void testEditArmyActuallyChangesValues() {
            try {
                Connection connection = Database.instance().getConnection();
                // Get the previous name of the army from the database.
                String previousName = connection.prepareStatement("SELECT army_name FROM armies WHERE army_id = " + armyWithId.getId()).executeQuery().getString(1);
                connection.close();
                armyDAO.editArmy(armyWithId);
                connection = Database.instance().getConnection();
                String newName = connection.prepareStatement("SELECT army_name FROM armies WHERE army_id = " + armyWithId.getId()).executeQuery().getString(1);
                assertNotEquals(previousName, newName);
                assertEquals(armyWithId.getName(), newName);
                connection.close();
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testEditArmyThrowsExceptionIfArmyIdIsZero() {
            assertThrows(IllegalArgumentException.class, () -> armyDAO.editArmy(armyWithOutId));
        }
    }

    @Nested
    class DeleteArmy {

        @Test
        void testDeleteArmyDeletesArmyAndArmyUnits() {
            try {
                armyDAO.deleteArmy(armyWithId);
                Connection connection = Database.instance().getConnection();
                ResultSet armyResultSet = connection.prepareStatement("SELECT * FROM armies WHERE army_id = " + armyWithId.getId()).executeQuery();
                ResultSet unitResultSet = connection.prepareStatement("SELECT * FROM units WHERE army_id = " + armyWithId.getId()).executeQuery();
                // Checks that the result set of the SQL query is empty.
                assertFalse(armyResultSet.next());
                assertFalse(unitResultSet.next());
                connection.close();
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testDeleteUnitThrowsExceptionIfIdIsZero() {
            assertThrows(IllegalArgumentException.class, () -> armyDAO.deleteArmy(armyWithOutId));
        }
    }
}
