package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class CavalryUnitTest {

    @Nested
    class CavalryAttackBonus {

        @Test
        void testDefaultCavalryUnitAttackBonusOnlyDiffersTwice() {
            Unit unit = UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "Cavalry", 10);
            int attackBonus1 = unit.getAttackBonus(TerrainType.HILL.getCode());
            int attackBonus2 = unit.getAttackBonus(TerrainType.HILL.getCode());
            int attackBonus3 = unit.getAttackBonus(TerrainType.HILL.getCode());

            assertTrue(attackBonus1 > attackBonus2);
            assertEquals(attackBonus2, attackBonus3);
        }

        @Test
        void testCavalryUnitAttackBonusIsGreaterInPlains() {
            Unit defaultCavalryUnit = UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "DefaultCavalry", 10);
            Unit plainsCavalryUnit = UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "PlainsCavalry", 10);
            int defaultCavalryUnitAttackBonus1 = defaultCavalryUnit.getAttackBonus(TerrainType.HILL.getCode());
            int defaultCavalryUnitAttackBonus2 = defaultCavalryUnit.getAttackBonus(TerrainType.HILL.getCode());
            int defaultCavalryUnitAttackBonus3 = defaultCavalryUnit.getAttackBonus(TerrainType.HILL.getCode());
            int plainsCavalryUnitAttackBonus1 = plainsCavalryUnit.getAttackBonus(TerrainType.PLAINS.getCode());
            int plainsCavalryUnitAttackBonus2 = plainsCavalryUnit.getAttackBonus(TerrainType.PLAINS.getCode());
            int plainsCavalryUnitAttackBonus3 = plainsCavalryUnit.getAttackBonus(TerrainType.PLAINS.getCode());
            assertTrue(plainsCavalryUnitAttackBonus1 > defaultCavalryUnitAttackBonus1);
            assertTrue(plainsCavalryUnitAttackBonus2 > defaultCavalryUnitAttackBonus2);
            assertTrue(plainsCavalryUnitAttackBonus3 > defaultCavalryUnitAttackBonus3);
            assertEquals(plainsCavalryUnitAttackBonus2, plainsCavalryUnitAttackBonus3);
        }
    }

    @Nested
    class CavalryResistBonus {

        @Test
        void testCavalryResistBonusIsZeroInForest() {
            Unit defaultCavalryUnit = UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "DefaultCavalry", 10);
            Unit forestCavalryUnit = UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "forestCavalry", 10);
            int defaultCavalryUnitResistBonus = defaultCavalryUnit.getResistBonus(TerrainType.HILL.getCode());
            int forestCavalryUnitResistBonus = forestCavalryUnit.getResistBonus(TerrainType.FOREST.getCode());
            assertTrue(forestCavalryUnitResistBonus < defaultCavalryUnitResistBonus);
            assertEquals(0, forestCavalryUnitResistBonus);
        }
    }
}
