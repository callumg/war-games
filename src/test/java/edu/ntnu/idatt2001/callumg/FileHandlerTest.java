package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import edu.ntnu.idatt2001.callumg.model.util.FileHandler;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

public class FileHandlerTest {

    @TempDir
    public Path temp;

    Army army;

    @Nested
    class ArmyExportToFileThrowsException {

        @Test
        void testExportToFileThrowsExceptionWhenFileNameIsInvalid() {
            Path tempFile = temp.resolve("invalid.txt");
            army = new Army(0, "Army");
            army.addAllUnits(
                Arrays.asList(
                    new CommanderUnit(0, "Commander", 10),
                    new InfantryUnit(0, "Infantry", 10),
                    new CommanderUnit(0, "Commander", 10),
                    new RangedUnit(0, "Ranged", 10),
                    new CavalryUnit(0, "Cavalry", 10),
                    new CommanderUnit(0, "Commander", 10)
                )
            );
            assertThrows(InvalidPathException.class, () -> FileHandler.exportToFile(army, tempFile.toString()));
        }

        @Test
        void testExportToFileDoesNotThrowExceptionWhenFileNameIsValid() {
            Path tempFile = temp.resolve("valid.csv");
            army = new Army(0, "Army");
            army.addAllUnits(
                Arrays.asList(
                    new CommanderUnit(0, "Commander", 10),
                    new InfantryUnit(0, "Infantry", 10),
                    new CommanderUnit(0, "Commander", 10),
                    new RangedUnit(0, "Ranged", 10),
                    new CavalryUnit(0, "Cavalry", 10),
                    new CommanderUnit(0, "Commander", 10)
                )
            );
            assertDoesNotThrow(() -> FileHandler.exportToFile(army, tempFile.toString()));
        }

        @Test
        void testExportToFileThrowsExceptionIfArmyIsNull() {
            Path tempFile = temp.resolve("valid.csv");
            army = null;
            assertThrows(IllegalArgumentException.class, () -> FileHandler.exportToFile(army, tempFile.toString()));
        }
    }

    @Nested
    class ArmyReadFromFileThrowsException {

        @Test
        void testReadFromFileThrowsExceptionIfFileNameIsInvalid() {
            Path tempFile = temp.resolve("invalid.txt");
            assertThrows(InvalidPathException.class, () -> FileHandler.readFromFile(tempFile.toString()));
        }

        @Test
        void testReadFromFileThrowsExceptionIfFileDoesNotContainArmyName() {
            File tempFile = new File(String.valueOf(temp), "tempFile.csv");
            try (FileWriter fw = new FileWriter(tempFile.getPath())) {
                fw.write('\n' + "InfantryUnit,Infantry,10");
            } catch (IOException e) {
                fail();
            }
            try {
                army = FileHandler.readFromFile(tempFile.getPath());
                fail();
            } catch (InvalidPathException e) {
                fail();
            } catch (IOException e) {
                assertEquals("Army name in file is blank.", e.getMessage());
            }
        }

        @Test
        void testReadFromFileThrowsExceptionIfFileContainsArmyNameWithSpecialCharacters() {
            File tempFile = new File(String.valueOf(temp), "tempFile.csv");
            try (FileWriter fw = new FileWriter(tempFile.getPath())) {
                fw.write("InfantryUnit,Infantry,10");
            } catch (IOException e) {
                fail();
            }
            try {
                army = FileHandler.readFromFile(tempFile.getPath());
                fail();
            } catch (InvalidPathException e) {
                fail();
            } catch (IOException e) {
                assertEquals("Army name in file contains special characters", e.getMessage());
            }
        }

        @Test
        void testReadFromFileThrowsExceptionIfFileContainsAWrongUnitType() {
            File tempFile = new File(String.valueOf(temp), "tempFile.csv");
            try (FileWriter fw = new FileWriter(tempFile.getPath())) {
                fw.write("ArmyName" + '\n' + "IAmNotAUnit,Infantry,10");
            } catch (IOException e) {
                fail();
            }
            try {
                army = FileHandler.readFromFile(tempFile.getPath());
                fail();
            } catch (InvalidPathException e) {
                fail();
            } catch (IOException e) {
                assertEquals("One of the units had a unit type that doesn't exist.", e.getMessage());
            }
        }

        @Test
        void testReadFromFileThrowsExceptionIfFileContainsAWrongLine() {
            File tempFile = new File(String.valueOf(temp), "tempFile.csv");
            try (FileWriter fw = new FileWriter(tempFile.getPath())) {
                fw.write("ArmyName" + '\n' + "InfantryUnit,Infantry,10" + '\n' + "InfantryUnit,10");
            } catch (IOException e) {
                fail();
            }
            try {
                army = FileHandler.readFromFile(tempFile.getPath());
                fail();
            } catch (InvalidPathException e) {
                fail();
            } catch (IOException e) {
                assertEquals("One or more lines in the file were corrupted.", e.getMessage());
            }
        }
    }

    @Nested
    class LargeAmounts {

        @Test
        void testWriteToFileCanWriteMassiveArmies() {
            Path tempFile = temp.resolve("valid.csv");
            List<Unit> units = new ArrayList<>();
            units.addAll(UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.CAVALRY_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.RANGED_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.COMMANDER_UNIT, "Unit", 10, 100000));
            army = new Army(0, "Army", units);
            try {
                FileHandler.exportToFile(army, tempFile.toString());
            } catch (IOException | InvalidPathException e) {
                fail();
            }
        }

        @Test
        void testReadFromFileCanReadMassiveArmies() {
            Path tempFile = temp.resolve("valid.csv");
            List<Unit> units = new ArrayList<>();
            units.addAll(UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.CAVALRY_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.RANGED_UNIT, "Unit", 10, 100000));
            units.addAll(UnitFactory.getManyUnits(UnitType.COMMANDER_UNIT, "Unit", 10, 100000));
            army = new Army(0, "Army", units);
            try {
                FileHandler.exportToFile(army, tempFile.toString());
            } catch (IOException | InvalidPathException e) {
                fail();
            }
            try {
                Army armyFromFile = FileHandler.readFromFile(tempFile.toString());
                assertEquals(army.getAllUnits().size(), armyFromFile.getAllUnits().size());
            } catch (IOException e) {
                fail();
            }
        }
    }

    @Test
    void testWriteToFileAndReadFromFileGetsAndGivesCorrectData() {
        String tempFile = temp.resolve("valid.csv").toString();
        List<Unit> units = new ArrayList<>();
        units.addAll(UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Unit", 10, 10));
        units.addAll(UnitFactory.getManyUnits(UnitType.CAVALRY_UNIT, "Unit", 10, 10));
        units.addAll(UnitFactory.getManyUnits(UnitType.RANGED_UNIT, "Unit", 10, 10));
        units.addAll(UnitFactory.getManyUnits(UnitType.COMMANDER_UNIT, "Unit", 10, 10));
        army = new Army(0, "Army", units);
        try {
            FileHandler.exportToFile(army, tempFile);
        } catch (IOException e) {
            fail();
        }
        try {
            Army armyFromFile = FileHandler.readFromFile(tempFile);
            assertEquals(army.getAllUnits().size(), armyFromFile.getAllUnits().size());
        } catch (IOException e) {
            fail();
        }
    }
}
