package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class WarUnitTest {

    @Nested
    class UnitConstructorThrowsException {

        Unit unit;

        @Test
        void testUnitCannotHaveBlankName() {
            try {
                unit = new InfantryUnit(0, " ", 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The name of the unit cannot be empty or blank.", e.getMessage());
            }
        }

        @Test
        void testUnitNameCannotContainSpecialCharacters() {
            try {
                unit = new InfantryUnit(0, "John,", 10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The name of the unit cannot contain special characters.", e.getMessage());
            }
        }

        @Test
        void testUnitCannotStartWithLessThanZeroHealth() {
            try {
                unit = new InfantryUnit(0, "Infantry", -100);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The health of the unit cannot be less than or equal to 0 at initiation.", e.getMessage());
            }
        }

        @Test
        void testUnitCannotHaveNegativeAttack() {
            try {
                unit = new InfantryUnit(0, "Infantry", 10, -10, 0);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The attack stat of the unit cannot be negative.", e.getMessage());
            }
        }

        @Test
        void testUnitCannotHaveNegativeArmor() {
            try {
                unit = new InfantryUnit(0, "Infantry", 10, 0, -10);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The armor stat of the unit cannot be negative.", e.getMessage());
            }
        }
    }

    @Test
    void testUnitDoesNotDealNegativeDamageIfTotalAttackIsLessThanTotalResistance() {
        Unit attacker = new InfantryUnit(0, "Attacker", 10, 0, 10);
        Unit defender = new InfantryUnit(0, "Defender", 10, 0, 10);

        int startHealth = defender.getHealth();
        attacker.attack(defender, 1);
        int endHealth = defender.getHealth();

        assertEquals(startHealth, endHealth);
    }
}
