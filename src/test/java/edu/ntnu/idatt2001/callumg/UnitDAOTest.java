package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.database.Database;
import edu.ntnu.idatt2001.callumg.model.database.UnitDAO;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class UnitDAOTest {

    // All units in the test database have the stats name: "Infantry", health: 10, attack: 10, armor: 10.
    Unit unitWithoutId = new InfantryUnit(0, "Infantry", 10, 10, 10);
    Unit unitWithId = new InfantryUnit(1, "Infantry Guy", 100, 100, 100);
    private UnitDAO unitDAO;

    @BeforeEach
    void setup() {
        unitWithoutId.setId(0);
        unitDAO = new UnitDAO();
        try {
            CreateTestDB.setUp();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Nested
    class GetUnits {

        @Test
        void testGetUnitsReturnsCorrectAmount() {
            try {
                List<Unit> units = unitDAO.getUnits(2);
                assertEquals(40, units.size());
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testGetUnitsReturnsCorrectTroops() {
            try {
                List<Unit> units = unitDAO.getUnits(2);
                // The way the insert statements are made,
                // the units are inserted as different for every unit and repeat every 4 inserts.
                // This will therefore check all units.
                for (int i = 0; i < 10; i += 4) {
                    assertEquals(UnitType.INFANTRY_UNIT.getType(), units.get(i).getClass().getSimpleName());
                    assertEquals(UnitType.RANGED_UNIT.getType(), units.get(i + 1).getClass().getSimpleName());
                    assertEquals(UnitType.CAVALRY_UNIT.getType(), units.get(i + 2).getClass().getSimpleName());
                    assertEquals(UnitType.COMMANDER_UNIT.getType(), units.get(i + 3).getClass().getSimpleName());
                }
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testGetUnitsReturnsSQLExceptionWhenArmyIdIsZero() {
            assertThrows(IllegalArgumentException.class, () -> unitDAO.getUnits(0));
        }
    }

    @Nested
    class AddUnit {

        @Test
        void testAddUnitGivesUnitTheCorrectIdAfterAddingItToTheDatabase() {
            try {
                unitDAO.addUnit(unitWithoutId, 1);
                Connection connection = Database.instance().getConnection();
                // This code counts the amount of rows and
                // checks it matches the new id as it the latest id always matches row unless an earlier id is deleted.
                int lastUnitId = connection.prepareStatement("SELECT count(unit_id) FROM units;").executeQuery().getInt(1);
                connection.close();
                assertEquals(lastUnitId, unitWithoutId.getId());
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testAddUnitAddsTheCorrectUnitToTheDatabase() {
            try {
                unitDAO.addUnit(unitWithoutId, 1);
                Connection connection = Database.instance().getConnection();
                int lastRowId = connection.prepareStatement("SELECT count(unit_id) FROM units;").executeQuery().getInt(1);
                // I know this is bad practice using + in strings and not using prepareStatement.setInt, but it is not necessary as
                // this is a test database and this code isn't interfaced by the user.
                ResultSet resultSet = connection.prepareStatement("SELECT * FROM units WHERE unit_id = " + lastRowId).executeQuery();
                assertEquals(unitWithoutId.getName(), resultSet.getString("unit_name"));
                assertEquals(unitWithoutId.getHealth(), resultSet.getInt("unit_health"));
                assertEquals(unitWithoutId.getAttack(), resultSet.getInt("unit_attack"));
                assertEquals(unitWithoutId.getArmor(), resultSet.getInt("unit_armor"));
                assertEquals(unitWithoutId.getClass().getSimpleName(), resultSet.getString("unit_type"));
                connection.close();
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testAddUnitThrowsExceptionIfIdAlreadyExists() {
            assertThrows(IllegalArgumentException.class, () -> unitDAO.addUnit(unitWithId, 1));
        }
    }

    @Nested
    class EditUnit {

        @Test
        void testEditUnitActuallyChangesValues() {
            try {
                unitDAO.editUnit(unitWithId);
                Connection connection = Database.instance().getConnection();
                ResultSet resultSet = connection.prepareStatement("SELECT * FROM units WHERE unit_id = " + unitWithId.getId()).executeQuery();
                assertEquals(unitWithId.getName(), resultSet.getString("unit_name"));
                assertEquals(unitWithId.getHealth(), resultSet.getInt("unit_health"));
                assertEquals(unitWithId.getAttack(), resultSet.getInt("unit_attack"));
                assertEquals(unitWithId.getArmor(), resultSet.getInt("unit_armor"));
                assertEquals(unitWithId.getClass().getSimpleName(), resultSet.getString("unit_type"));
                connection.close();
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testEditUnitThrowsExceptionIfIdIsZero() {
            // I have the set the id to 0, the unitId will never be set unless the unit is fetched from the database.
            assertThrows(IllegalArgumentException.class, () -> unitDAO.editUnit(unitWithoutId));
        }
    }

    @Nested
    class DeleteUnit {

        @Test
        void testDeleteUnitDeletesUnit() {
            try {
                unitDAO.deleteUnit(unitWithId);
                Connection connection = Database.instance().getConnection();
                ResultSet resultSet = connection.prepareStatement("SELECT * FROM units WHERE unit_id = " + unitWithId.getId()).executeQuery();
                // Checks that the result set of the SQL query is empty.
                assertEquals(false, resultSet.next());
                connection.close();
            } catch (SQLException e) {
                fail();
            }
        }

        @Test
        void testDeleteUnitThrowsExceptionIfIdIsZero() {
            assertThrows(IllegalArgumentException.class, () -> unitDAO.deleteUnit(unitWithoutId));
        }
    }
}
