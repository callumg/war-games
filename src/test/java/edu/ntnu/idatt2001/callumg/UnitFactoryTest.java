package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class UnitFactoryTest {

    @Nested
    class UnitFactoryDoesNotThrowException {

        @Test
        void testUnitFactoryGetInfantryUnitDoesNotThrowIllegalArgument() {
            assertDoesNotThrow(() -> UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Name", 10));
        }

        @Test
        void testUnitFactoryGetRangedUnitDoesNotThrowIllegalArgument() {
            assertDoesNotThrow(() -> UnitFactory.getUnit(UnitType.RANGED_UNIT, "Name", 10));
        }

        @Test
        void testUnitFactoryGetCavalryUnitDoesNotThrowIllegalArgument() {
            assertDoesNotThrow(() -> UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "Name", 10));
        }

        @Test
        void testUnitFactoryGetCommanderUnitDoesNotThrowIllegalArgument() {
            assertDoesNotThrow(() -> UnitFactory.getUnit(UnitType.COMMANDER_UNIT, "Name", 10));
        }

        @Test
        void testUnitFactoryGetUnitThrowsExceptionWhenNameIsWrong() {
            assertThrows(IllegalArgumentException.class, () -> UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "", 10));
        }

        @Test
        void testUnitFactoryGetUnitThrowsExceptionWhenHealthIsInvalid() {
            assertThrows(IllegalArgumentException.class, () -> UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Name", -10));
        }
    }

    @Nested
    class UnitFactoryGetUnitReturnsCorrectUnit {

        @Test
        void testUnitFactoryGetInfantryUnit() {
            assertEquals(InfantryUnit.class, UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Name", 10).getClass());
        }

        @Test
        void testUnitFactoryGetRangedUnit() {
            assertEquals(RangedUnit.class, UnitFactory.getUnit(UnitType.RANGED_UNIT, "Name", 10).getClass());
        }

        @Test
        void testUnitFactoryGetCavalryUnit() {
            assertEquals(CavalryUnit.class, UnitFactory.getUnit(UnitType.CAVALRY_UNIT, "Name", 10).getClass());
        }

        @Test
        void testUnitFactoryGetCommanderUnit() {
            assertEquals(CommanderUnit.class, UnitFactory.getUnit(UnitType.COMMANDER_UNIT, "Name", 10).getClass());
        }
    }

    @Nested
    class UnitFactoryGetManyUnits {

        @Test
        void testUnitFactoryGetManyUnitsThrowsExceptionWhenAmountIsZero() {
            try {
                UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Name", 10, 0);
            } catch (IllegalArgumentException e) {
                assertEquals("You cannot create 0 or less troops.", e.getMessage());
            }
        }

        @Test
        void testUnitFactoryGetManyUnitsThrowsExceptionWhenNameIsWrong() {
            assertThrows(IllegalArgumentException.class, () -> UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "", 10, 10));
        }

        @Test
        void testUnitFactoryGetManyUnitsThrowsExceptionWhenHealthIsInvalid() {
            assertThrows(IllegalArgumentException.class, () -> UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Name", -10, 10));
        }

        @Test
        void testUnitFactoryGetManyUnitsReturnsCorrectList() {
            List<Unit> list = UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Name", 10, 10);
            assertEquals(10, list.size());
            list.forEach(unit -> assertTrue(unit.getClass().equals(InfantryUnit.class)));
        }
    }
}
