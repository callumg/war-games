package edu.ntnu.idatt2001.callumg;

import edu.ntnu.idatt2001.callumg.model.database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTestDB {

    private static Connection connection;
    private static Statement statement;
    private static PreparedStatement preparedStatement;

    public static void setUp() throws SQLException {
        connection = Database.instance().getConnection();
        statement = connection.createStatement();

        String dropArmies = "DROP TABLE IF EXISTS armies";
        String dropUnits = "DROP TABLE IF EXISTS units";

        String armySql = "CREATE TABLE IF NOT EXISTS armies (\n" + "	army_id integer PRIMARY KEY,\n" + "	army_name text\n" + ");";

        String unitSql =
            "CREATE TABLE IF NOT EXISTS units (\n" +
            " unit_id integer PRIMARY KEY,\n" +
            " army_id integer,\n" +
            " unit_name text,\n" +
            " unit_health int,\n" +
            " unit_attack int,\n" +
            " unit_armor int,\n" +
            " unit_type text,\n" +
            " FOREIGN KEY (army_id) REFERENCES armies(army_id)\n" +
            ");";

        String insertInfantryUnit = "INSERT INTO units (army_id, unit_name, unit_health, unit_attack, unit_armor, unit_type) "
                + "VALUES (?, 'Infantry', 10, 10, 10, 'InfantryUnit')";
        String insertRangedUnit = "INSERT INTO units (army_id, unit_name, unit_health, unit_attack, unit_armor, unit_type) "
                + "VALUES (?, 'Ranged', 10, 10, 10, 'RangedUnit')";
        String insertCavalryUnit = "INSERT INTO units (army_id, unit_name, unit_health, unit_attack, unit_armor, unit_type) "
                + "VALUES (?, 'Cavalry', 10, 10, 10, 'CavalryUnit')";
        String insertCommanderUnit = "INSERT INTO units (army_id, unit_name, unit_health, unit_attack, unit_armor, unit_type) "
                + "VALUES (?, 'Commander', 10, 10, 10, 'CommanderUnit')";
        String insertArmy1 = "INSERT INTO armies (army_name) VALUES ('Army One')";
        String insertArmy2 = "INSERT INTO armies (army_name) VALUES ('Army Two')";
        String insertArmy3 = "INSERT INTO armies (army_name) VALUES ('Army Three')";
        String insertArmy4 = "INSERT INTO armies (army_name) VALUES ('Army Four')";

        try {
            statement.executeUpdate(dropUnits);
            statement.executeUpdate(dropArmies);

            statement.executeUpdate(unitSql);
            statement.executeUpdate(armySql);

            statement.executeUpdate(insertArmy1);
            statement.executeUpdate(insertArmy2);
            statement.executeUpdate(insertArmy3);
            statement.executeUpdate(insertArmy4);

            int armyId = 1;
            for (int i = 0; i < 40; i++) {
                if (i != 0 && i % 10 == 0) {
                    armyId++;
                }
                preparedStatement = connection.prepareStatement(insertInfantryUnit);
                preparedStatement.setInt(1, armyId);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement(insertRangedUnit);
                preparedStatement.setInt(1, armyId);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement(insertCavalryUnit);
                preparedStatement.setInt(1, armyId);
                preparedStatement.executeUpdate();
                preparedStatement = connection.prepareStatement(insertCommanderUnit);
                preparedStatement.setInt(1, armyId);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
        } finally {
            statement.close();
        }
    }
}
