package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.CommanderUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.InfantryUnit;
import edu.ntnu.idatt2001.callumg.model.units.specialized.RangedUnit;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ArmyTest {

    Army army;

    private Army createListWithRandomTotalOfUnits(int unitCount, UnitType unitType) {
        ArrayList<Unit> unitList = new ArrayList<>();
        unitList.addAll(UnitFactory.getManyUnits(unitType, "Unit", 10, unitCount));
        army = new Army(0, "Army", unitList);

        return army;
    }

    @Nested
    class ArmyConstructorThrowsException {

        @Test
        void testArmyCannotHaveBlankName() {
            try {
                army = new Army(0, " ");
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("The name of the army cannot be blank.", e.getMessage());
            }
        }

        @Test
        void testArmyCanHaveExistWithoutUnits() {
            army = new Army(0, "Army");

            assertNotNull(army.getAllUnits());
            assertFalse(army.hasUnits());
        }

        @Test
        void testArmyStoresUnitsWhenGivenList() {
            int unitCount = ThreadLocalRandom.current().nextInt(1, 10 + 1);

            army = createListWithRandomTotalOfUnits(unitCount, UnitType.INFANTRY_UNIT);

            assertEquals(unitCount, army.getAllUnits().size());
            assertTrue(army.hasUnits());
        }
    }

    @Nested
    class ArmyGetMethodsReturnCorrectUnits {

        @Test
        void testGetRandomTroopFromArmyReturnsCorrectTypeOfUnit() {
            int unitCount = ThreadLocalRandom.current().nextInt(1, 10 + 1);
            army = createListWithRandomTotalOfUnits(unitCount, UnitType.INFANTRY_UNIT);
            Unit unit = new InfantryUnit(0, "Unit", 10);
            Unit randomUnit = army.getRandom();

            assertEquals(unit.getClass().getSimpleName(), randomUnit.getClass().getSimpleName());
        }

        @Test
        void testGetRandomTroopFromArmyReturnsFalseWhenEmpty() {
            army = new Army(0, "Army");

            assertNull(army.getRandom());
        }

        @Nested
        class GetTroopsOfCertainTypeReturnValues {

            @BeforeEach
            void addTroopsToArmy() {
                army = new Army(0, "Army");
                army.addAllUnits(UnitFactory.getManyUnits(UnitType.INFANTRY_UNIT, "Unit", 10, 5));
                army.addAllUnits(UnitFactory.getManyUnits(UnitType.RANGED_UNIT, "Unit", 10, 5));
                army.addAllUnits(UnitFactory.getManyUnits(UnitType.CAVALRY_UNIT, "Unit", 10, 5));
                army.addAllUnits(UnitFactory.getManyUnits(UnitType.COMMANDER_UNIT, "Unit", 10, 5));
            }

            @Test
            void testGetInfantryUnitsFromArmyReturnsOnlyInfantryUnits() {
                List<Unit> infantryUnits = army.getInfantryUnits();
                assertEquals(5, infantryUnits.size());
                assertTrue(infantryUnits.stream().allMatch(unit -> unit.getClass().equals(new InfantryUnit(0, "Unit", 10).getClass())));
            }

            @Test
            void testGetRangedUnitsFromArmyReturnsOnlyRangedUnits() {
                List<Unit> rangedUnits = army.getRangedUnits();
                assertEquals(5, rangedUnits.size());
                assertTrue(rangedUnits.stream().allMatch(unit -> unit.getClass().equals(new RangedUnit(0, "Unit", 10).getClass())));
            }

            @Test
            void testGetCavalryUnitsFromArmyReturnsOnlyCavalryUnits() {
                List<Unit> cavalryUnits = army.getCavalryUnits();
                assertEquals(5, cavalryUnits.size());
                assertTrue(cavalryUnits.stream().allMatch(unit -> unit.getClass().equals(new CavalryUnit(0, "Unit", 10).getClass())));
            }

            @Test
            void testGetCommanderUnitsFromArmyReturnsOnlyCommanderUnits() {
                List<Unit> commanderUnits = army.getCommanderUnits();
                assertEquals(5, commanderUnits.size());
                assertTrue(commanderUnits.stream().allMatch(unit -> unit.getClass().equals(new CommanderUnit(0, "Unit", 10).getClass())));
            }
        }
    }

    @Test
    void testArmyCanAddUnit() {
        army = new Army(0, "Army");
        assertTrue(army.addUnit(UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Unit", 10)));
    }

    @Test
    void testRemoveNonExistingUnitFromArmyReturnsFalse() {
        army = new Army(0, "Army");
        assertFalse(army.removeUnit(UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Unit", 10)));
    }

    @Test
    void testArmiesHaveDifferentHashCodesUnlessTheyAreCopies() {
        ArrayList<Unit> unitList1 = new ArrayList<>();
        ArrayList<Unit> unitList2 = new ArrayList<>();
        ArrayList<Unit> unitList3 = new ArrayList<>();

        unitList1.add(new InfantryUnit(0, "Infantry", 10));
        unitList2.add(new InfantryUnit(0, "Infantry", 10));
        unitList3.add(new InfantryUnit(0, "Infantry", 10));

        Army armyOne = new Army(0, "Army", unitList1);
        Army armyTwo = new Army(0, "Army", unitList2);
        Army armyThree = new Army(0, "Army", unitList3);
        Army armyFour = new Army(0, "Army", unitList3);

        assertFalse(armyOne.hashCode() == armyTwo.hashCode());
        assertEquals(armyThree.hashCode(), armyFour.hashCode());
    }
}
