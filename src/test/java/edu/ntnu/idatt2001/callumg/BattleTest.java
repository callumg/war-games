package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.Army;
import edu.ntnu.idatt2001.callumg.model.Battle;
import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class BattleTest {

    Battle battle;
    Army armyWithoutUnits;
    Army armyWithUnits;

    @BeforeEach
    void initializeArmies() {
        ArrayList<Unit> unitList = new ArrayList<>();
        unitList.add(UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Unit", 10));
        armyWithoutUnits = new Army(0, "ArmyOne");
        armyWithUnits = new Army(0, "ArmyTwo", unitList);
    }

    @Nested
    class BattleConstructorThrowsException {

        @Test
        void testBothArmiesMustHaveTroops() {
            try {
                battle = new Battle(armyWithoutUnits, armyWithUnits, TerrainType.FOREST);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("An army cannot be empty.", e.getMessage());
            }
        }

        @Test
        void testBothArmiesCannotBeEqual() {
            try {
                battle = new Battle(armyWithUnits, armyWithUnits, TerrainType.FOREST);
                fail();
            } catch (IllegalArgumentException e) {
                assertEquals("Fighting armies cannot be the same.", e.getMessage());
            }
        }
    }

    @Test
    void testSimulateGivesCorrectWinner() {
        Army armyTwo = new Army(0, "Army Two");
        armyTwo.addUnit(UnitFactory.getUnit(UnitType.INFANTRY_UNIT, "Unit", 10));

        battle = new Battle(armyWithUnits, armyTwo, TerrainType.FOREST);
        Army winner = battle.simulate();

        assertTrue(winner.hasUnits());
        if (!winner.equals(armyTwo)) assertFalse(armyTwo.hasUnits()); else assertFalse(armyWithUnits.hasUnits());
    }
}
