package edu.ntnu.idatt2001.callumg;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.callumg.model.units.Unit;
import edu.ntnu.idatt2001.callumg.model.util.TerrainType;
import edu.ntnu.idatt2001.callumg.model.util.UnitFactory;
import edu.ntnu.idatt2001.callumg.model.util.UnitType;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class RangedUnitTest {

    Unit unit;

    @Nested
    class RangedUnitAttackBonus {

        @Test
        void testRangedUnitAttackBonusHigherInHills() {
            Unit defaultRangedUnit = UnitFactory.getUnit(UnitType.RANGED_UNIT, "DefaultRanged", 10);
            Unit hillRangedUnit = UnitFactory.getUnit(UnitType.RANGED_UNIT, "HillRanged", 10);
            int defaultRangedUnitAttackBonus = defaultRangedUnit.getAttackBonus(TerrainType.PLAINS.getCode());
            int hillRangedUnitAttackBonus = hillRangedUnit.getAttackBonus(TerrainType.HILL.getCode());
            assertTrue(hillRangedUnitAttackBonus > defaultRangedUnitAttackBonus);
            assertEquals(defaultRangedUnitAttackBonus + 2, hillRangedUnitAttackBonus);
        }

        @Test
        void testRangedUnitAttackBonusLowerInForest() {
            Unit defaultRangedUnit = UnitFactory.getUnit(UnitType.RANGED_UNIT, "DefaultRanged", 10);
            Unit forestRangedUnit = UnitFactory.getUnit(UnitType.RANGED_UNIT, "ForestRanged", 10);
            int defaultRangedUnitAttackBonus = defaultRangedUnit.getAttackBonus(TerrainType.PLAINS.getCode());
            int forestRangedUnitAttackBonus = forestRangedUnit.getAttackBonus(TerrainType.FOREST.getCode());
            assertTrue(forestRangedUnitAttackBonus < defaultRangedUnitAttackBonus);
            assertEquals(defaultRangedUnitAttackBonus - 2, forestRangedUnitAttackBonus);
        }
    }

    @Nested
    class RangedUnitResistBonus {

        @Test
        void testRangedUnitResistBonusOnlyDiffersThreeTimes() {
            unit = UnitFactory.getUnit(UnitType.RANGED_UNIT, "Unit", 10);
            int resistBonus1 = unit.getResistBonus(TerrainType.HILL.getCode());
            int resistBonus2 = unit.getResistBonus(TerrainType.HILL.getCode());
            int resistBonus3 = unit.getResistBonus(TerrainType.HILL.getCode());
            int resistBonus4 = unit.getResistBonus(TerrainType.HILL.getCode());

            assertTrue(resistBonus1 > resistBonus2);
            assertTrue(resistBonus2 > resistBonus3);
            assertEquals(resistBonus3, resistBonus4);
        }
    }
}
